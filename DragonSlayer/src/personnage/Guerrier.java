package personnage;

/**
 * Created by Romano on 28/05/2016.
 */
public class Guerrier extends TypePerso {


    public Guerrier(){
        super();
        force = 8;
        dexterite = 4;
        constitution = 2;
        vie = 100;

        inventaire = new Objet[5];
        nbObjet = 0;
        /* ICI Creation des attaques du Guerrier ICI */
        Attack attaqueMain = new Attack("Attaque","Attaque basique du Guerrier, fais perdre 15pv à l'adversaire",-15, 0);
        Attack skill1 = new Attack("Charge","Charge l’ennemi et peut l’assommer pendant 1 tour",-30, 0);
        sesCompetences.add(attaqueMain);
        sesCompetences.add(skill1);
    }

    public String getJsonSkill() {
        String skillPerso = "";

        for (Skill mySkill : getSesCompetences()) {
            if(mySkill.getTour_A_Attendre() == 0)
            skillPerso += "\"" + mySkill.getSkillName() + "\",";
        }
        return skillPerso.substring(0, skillPerso.lastIndexOf(','));
    }

    public String getJsonObjet() {
        String objetPerso = "";
        Objet myObjet;

        for (int i = 0; i < nbObjet; i++) {
            myObjet =  getInventaire()[i];
            if (!(myObjet==null))
                objetPerso += "\"" + myObjet.getObjetName() + "\",";
        }

        if (nbObjet == 0) {
            return "";
        }
        else
            return objetPerso.substring(0, objetPerso.lastIndexOf(','));
    }

    public static void main(String args[]){
        Guerrier patata = new Guerrier();
        // Anton, t'aurais pas un moyen d'avoir accès directement à la liste des competences sans rentrer get(index)
        // Au lieu de rentrer get(0), on rentrerait plutot get(attaqueMain) ?
        patata.sesCompetences.get(0);
    }

}
