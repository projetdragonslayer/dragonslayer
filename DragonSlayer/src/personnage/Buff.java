package personnage;

/**
 * Created by Romano on 07/05/2016.
 */
public class Buff extends Skill {
    /* Attributs */
    int tour_A_Attendre;

    // nomCaracABooster correspond à la caractéristique à buffer
    public String nomCaracABooster;

    // qteCaracABooster correspond à la valeur à booster
    public int qteCaracABooster;

    // nbtours correspond au nb de tours où le buff est présent
    public int nbtours;



    /* Constructeur */
    public Buff(String skillName, String description,String nomCaracABooster,int qteCaracABooster, int nbtours) {
        super(skillName, description);
        this.nomCaracABooster = nomCaracABooster;
        this.qteCaracABooster = qteCaracABooster;
        this.nbtours = nbtours;
        buff = true;
        tour_A_Attendre = 0;
    }

    /* Methodes */

   /* private void appliquerBuff(Personnage nomduPersonnage){
        while (nbtours > 0){
            switch (nomCaracABooster){
                case "force": nomduPersonnage.sonTypePerso.force += this.qteCaracABooster;
                    break;
                case "dexterite": nomduPersonnage.sonTypePerso.dexterite += this.qteCaracABooster;
                    break;
                case "constitution": nomduPersonnage.sonTypePerso.constitution += this.qteCaracABooster;
                    break;
                case "vie": nomduPersonnage.sonTypePerso.vie += this.qteCaracABooster;
                    break;
            }
            nbtours--;
        }
    }*/
   @Override
   public boolean useSkill(TypePerso target) {
       /* Pour l'instant j'ai pas réusi à voir comment on peut buff une caractéristique d'un perso "simplement".
       Il faut prendre en compte le nombre de tours où le buff est valable ; la caractérisitique à buff (force, dexterité, constitution,??vie??)
       */
       /*while (nbtours > 0){
           switch (nomCaracABooster){
               case "force": nomduPersonnage.sonTypePerso.force += this.qteCaracABooster;
                   break;
               case "dexterite": nomduPersonnage.sonTypePerso.dexterite += this.qteCaracABooster;
                   break;
               case "constitution": nomduPersonnage.sonTypePerso.constitution += this.qteCaracABooster;
                   break;
               case "vie": nomduPersonnage.sonTypePerso.vie += this.qteCaracABooster;
                   break;
           }
           nbtours--;
       }*/
        return false;
   }

    public void tourUp() {
        if (tour_A_Attendre > 0)
            tour_A_Attendre += -1;
    }

    /* Exemple d'utilisation */
    public static void main(String args[]){
        Buff unBuffdeForceExemple = new Buff ("Renforcement","Ce buff va augmenter votre force pendant 3 tours","leSkillaBuff",10,3);
    }



}
