package personnage;

/**
 * Created by Romano on 28/05/2016.
 */
public class Mage extends TypePerso {

    public Mage(){
        super();
        force = 2;
        dexterite = 4;
        constitution = 8;
        vie = 100;

        inventaire = new Objet[5];
        nbObjet = 0;
        /* ICI Creation des attaques du Mage ICI */
        Attack attaqueMain = new Attack("Attaque à la baguette","Attaque basique du Mage, fais perdre 15pv à l'adversaire",-15,0);
        sesCompetences.add(attaqueMain); // Index = 0
        /* ICI Creation des Heals du Mage ICI */
        Heal petitHeal = new Heal("Petit Soin","ce soin permet de soigner le joueur en target de 10pv",10);
        sesCompetences.add(petitHeal); // Index = 1
    }

    // Méthode
    /* Ici, on aurait une liste de méthodes qui permettent à un Mage d'attaquer / healer */
    /*public void attaqueMain(TypePerso target){
        this.sesCompetences.get(0).useSkill(target);
    }
    public void soinPetit(TypePerso target){
        this.sesCompetences.get(1).useSkill(target);
    }
    public void soinPetit(){
        this.sesCompetences.get(1).useSkill(this);
    }*/

    public String getJsonSkill() {
        String skillPerso = "";

        for (Skill mySkill : getSesCompetences()) {
            if(mySkill.getTour_A_Attendre() == 0)
                skillPerso += "\"" + mySkill.getSkillName() + "\",";
        }
        return skillPerso.substring(0, skillPerso.lastIndexOf(','));
    }


    public String getJsonObjet() {
        String objetPerso = "";
        Objet myObjet;

        for (int i = 0; i < nbObjet; i++) {
            myObjet =  getInventaire()[i];
            if (!(myObjet==null))
                objetPerso += "\"" + myObjet.getObjetName() + "\",";
        }

        if (nbObjet == 0) {
            return "";
        }
        else
            return objetPerso.substring(0, objetPerso.lastIndexOf(','));
    }

    public static void main(String args[]){
        Mage patato = new Mage();
        // leMechant sert de target de test
        Guerrier leMechant = new Guerrier();
        //allie sert de joueur amical
        Guerrier allie = new Guerrier();

        // Patato veut lancer son attaque sur leMechant
        /*patato.attaqueMain(leMechant);

        // Patato veut soigner un allié
        patato.soinPetit(allie);

        // Patato veut se soigner
        patato.soinPetit();*/
    }


}
