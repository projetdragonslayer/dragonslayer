package personnage;

/**
 * Created by Romano on 07/05/2016.
 */
public class Heal extends Skill {
    // hp permet de connaitre le nb de points de vie à ajouter au perso
    int hp;

    public Heal(String skillName, String description,int hp) {
        super(skillName, description);
        this.hp = hp;
        heal = true;
        tour_A_Attendre = 0;
    }

    @Override
    public boolean useSkill(TypePerso target) {
        System.out.println("Avant le heal, : "+target.vie);

        if (tour_A_Attendre == 0) {
            if (target.vie < 100) {
                int result = target.vie + this.getHp();
                if (result > 100)
                    target.vie = 100;
                else
                    target.vie = result;

                System.out.println("Apres le heal, : "+target.vie);
                return true;
            }
        }
        System.out.println("Apres le heal, : "+target.vie);
        return false;
    }

    public void tourUp() {
        if (tour_A_Attendre > 0)
            tour_A_Attendre += -1;
    }

    public int getHp() {
        return hp;
    }

    public static void main(String args[]){
        /* Exemple de l'utilisation d'une attaque  */
        Heal petitSoin =  new Heal("Petit Soin","Rend 15 PV au personnage selectionné",15);
    }
}
