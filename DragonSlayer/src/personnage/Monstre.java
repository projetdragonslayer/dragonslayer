package personnage;

import java.util.ArrayList;

/**
 * Created by Sandy on 27/05/2016.
 */
public class Monstre extends TypePerso{
    private String nomMonstre;
    private int xpDrop;
    int l;
    int c;
    int vieDeBase;

    public Monstre(String nom, int force, int dexterite, int constitution, int vie, int xpDrop) {
        nomMonstre = nom;
        this.force = force;
        this.dexterite = dexterite;
        this.constitution = constitution;
        this.vie = vie;
        vieDeBase = vie;
        this.xpDrop = xpDrop;
    }

    public Monstre(Monstre unMonstre) {
        nomMonstre = unMonstre.getNomMonstre();
        this.force = unMonstre.force;
        this.dexterite = unMonstre.dexterite;
        this.constitution = unMonstre.constitution;
        this.vie = unMonstre.getVie();
        vieDeBase = this.vie;
        this.xpDrop = unMonstre.getXpDrop();
        this.setSesCompetences(unMonstre.getSesCompetences());
    }

    public ArrayList<Skill> getSesCompetences() {
        return sesCompetences;
    }

    public String getNomMonstre(){
        return nomMonstre;
    }

    public void setSesCompetences(ArrayList<Skill> sesCompetences) {
        this.sesCompetences = sesCompetences;
    }

    public int getXpDrop() {
        return xpDrop;
    }

    public int getL(){
        return l;
    }

    public int getC() {
        return c;
    }

    public void setPos(int l, int c) {
        this.l = l;
        this.c = c;
    }

    public String getJsonSkill() {
        String skillMonstre = "";

        for (Skill mySkill : getSesCompetences()) {
            if (mySkill.getTour_A_Attendre() == 0)
                skillMonstre += "\"" + mySkill.getSkillName() + "\",";
        }
        return skillMonstre.substring(0, skillMonstre.lastIndexOf(','));
    }

    public String getJsonObjet() {
        return null;
    }

    public int getVieDeBase() {
        return vieDeBase;
    }
}
