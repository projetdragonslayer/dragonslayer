package personnage;

/**
 * Created by Romano on 07/05/2016.
 *
 * Pour rappel, ici on place :
 * nom de competence (SkillName)
 * niveau de competence (SkillLevel)
 *
 *
 * Methode :
 * useSkill(Skillname,targetname)
 *
 */
public abstract class Skill {
    String skillName;
    String skillDesciption;
    int skillLevel;
    public boolean attack;
    public boolean buff;
    public boolean heal;
    int tour_A_Attendre;
    int nbtours;

    // Constructeur
    public Skill(String skillName,String description){
        this.skillName = skillName;
        this.skillDesciption=description;
        this.skillLevel= 1;
        attack = false;
        buff = false;
        heal = false;
    }

    public String getSkillName(){
        return skillName;
    }

    // Méthode abstraite
    /* useSkill s'utilisera sur chaque objet Attack / Heal. Buff n'est pas encore implementé */
    public abstract boolean useSkill(TypePerso target);

    public int getTour_A_Attendre() {
        return tour_A_Attendre;
    }

    public abstract void tourUp();
}
