package personnage;

import java.util.ArrayList;

/**
 * Created by Romano on 07/05/2016.
 *
 * Pour rappel, ici on place :
 * Force
 * Dexterite
 * Constitution
 * (cf. doc-competences.doc)
 *
 * Vie
 */

public abstract class TypePerso {
    int force;
    int dexterite;
    int constitution;
    int vie;

    Objet inventaire[];
    int nbObjet;
    ArrayList<Skill> sesCompetences = new ArrayList();

    public ArrayList<Skill> getSesCompetences() {
        return sesCompetences;
    }
    public int getVie() {
        return vie;
    }

    public Skill getSkillByName(String nomSkill) {
        int i = 0;
        boolean ok = false;
        Skill tempSkill = null;

        while (i < sesCompetences.size() && !ok) {
            tempSkill = sesCompetences.get(i);
            if (!(tempSkill == null)) {
                if (tempSkill.getSkillName().equals(nomSkill))
                    ok = true;
            }
            i++;
        }

        if (ok)
            return tempSkill;
        else
            return null;
    }

    public void addObjet(Objet unObjet) {
        if (nbObjet < inventaire.length) {
            inventaire[nbObjet] = unObjet;
            nbObjet ++;
        }
    }

    public void removeObjet(Objet unObjet) {
        int i = 0;
        boolean ok = false;
        Objet tempObjet = null;

        while (i < nbObjet && !ok) {
            tempObjet = inventaire[i];
            if (tempObjet.equals(unObjet)) {
                inventaire[i] = null;
                if (i == nbObjet -1)
                    nbObjet = nbObjet -1;
                else {
                    for (int j = i + 1; j < nbObjet; j++) {
                        inventaire[j - 1] = inventaire[j];
                    }
                    nbObjet = nbObjet - 1;
                }
                ok = true;
            }
            i++;
        }
    }

    public Objet getObjetByName(String objetName) {
        int i = 0;
        boolean ok = false;
        Objet tempObjet = null;

        while (i < nbObjet && !ok) {
            tempObjet = inventaire[i];
            if (tempObjet.getObjetName().equals(objetName))
                ok = true;
            i++;
        }

        if (ok)
            return tempObjet;
        else
            return null;
    }

    public Objet[] getInventaire() {
        return inventaire;
    }

    public int getAttribut(String nomAttribut) {
        int val = -9999;

        if (nomAttribut.equals("force")) {
            val = force;
        }
        else if (nomAttribut.equals("dexterite")) {
            val = dexterite;
        }
        else if (nomAttribut.equals("constitution")) {
            val = constitution;
        }
        else if (nomAttribut.equals("vie")) {
            val = vie;
        }
        return val;
    }

    public void setVie(int vie) {
        this.vie = vie;
    }

    public abstract String getJsonSkill();

    public abstract String getJsonObjet();
}
