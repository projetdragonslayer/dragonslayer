package personnage;

import model.Combat;
import model.ModelServeur;

public class Personnage {
	private String nom;
	private int direction;
	private int x;
	private int y;
	private int xp;
	private int lvl;

	private TypePerso classePerso;

	//Attribut utilisé en combat
	private Combat unCombat;
	private boolean enCombat;
	private boolean actionFaite;

	public Personnage (String nom) {
		this.nom = nom;
		direction = ModelServeur.BAS;
		x = 18;
		y = 1;
		enCombat = false;
	}

	public Personnage (String nom, String classe, int lvl, int xp) {
		this.nom = nom;
		direction = ModelServeur.BAS;
		x = 18;
		y = 1;
		enCombat = false;
		if (classe.toLowerCase().equals("guerrier")) {
			classePerso = new Guerrier();
		}
		else if (classe.toLowerCase().equals("mage")) {
			classePerso = new Mage();
		}
		else if (classe.toLowerCase().equals("voleur")) {
			classePerso = new Voleur();
		}
		this.lvl = lvl;
		this.xp = xp;
		switch (lvl) { //Pas encore pris en compte
			case 1:
				break;
			case 2:
				break;
			case 3:
				break;
			case 4:
				break;
			case 5:
				break;
		}
	}
	/*
	A METTRE LORSQUE LA BDD SERA FINI
	public Personnage (String nom, int x, int y, LE RESTE CEST LINSTANCIATION DE TYPE PERSO) {
		this.nom = nom;
		direction = ModelServeur.BAS;
		this.x = x;
		this.y = y;
		enCombat = false;
	}
	 */
	
	public void setPos(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public int getDirection() {
		return direction;
	}
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public TypePerso getTypePerso() {
		return classePerso;
	}

	public String getNomJoueur() {
		return nom;
	}

	public void setCombat(Combat unCombat) {
		this.unCombat = unCombat;

		if (unCombat == null)
			enCombat = false;
		else
			enCombat = true;
	}

	public Combat getCombat() {
		return unCombat;
	}

	public void addXp(int nbXp) {
		int result = xp + nbXp;
		if (result >= 100) {
			//lvlUp();
			lvl += 1;
			xp = result - 100;
		}
		else if (result <= 0) {
			xp = 0;
		}
		else
			xp = result;
	}

	public static void main (String args[]){
		/* Exemple de perso */
		Personnage prmeierPerso = new Personnage("1erPerso");
	}
}
