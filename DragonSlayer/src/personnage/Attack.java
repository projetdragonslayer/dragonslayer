package personnage;

/**
 * Created by Romano on 07/05/2016.
 */
public class Attack extends Skill {
    public int hp;

    public Attack(String skillName, String description,int hp, int nbTour) {
        super(skillName, description);
        this.hp = hp;
        attack = true;
        this.nbtours = nbTour;
        tour_A_Attendre = 0;
    }

    @Override
    public boolean useSkill(TypePerso target) {
        System.out.println("Avant attaque, : "+target.vie);
        if (tour_A_Attendre == 0) {
            int result = target.vie + this.hp;
            if (result >= 0) {
                target.vie = result;
            }
            else
                target.vie = 0;
            tour_A_Attendre = nbtours;
            return true;
        }
        System.out.println("Apres attaque, : "+target.vie);
        return false;
    }

    public void tourUp() {
        if (tour_A_Attendre > 0)
            tour_A_Attendre += -1;
    }

    public static void main(String args[]){
        /* Exemple de l'utilisation d'une attaque  */
        Attack attaqueDuChasseur =  new Attack("Attaque du chasseur","Cette attaque enlève 10 PV à l'adversaire",-10, 0);
    }
}
