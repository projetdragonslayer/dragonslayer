package personnage;

/**
 * Created by Sandy on 01/06/2016.
 */
public class Objet {
    private String nomObjet;
    private String description;
    private int pv;
    //private int mana;

    public Objet(String nom, String description, int pv) {
        nomObjet = nom;
        this.description = description;
        this.pv = pv;
    }

    public String getObjetName() {
        return nomObjet;
    }

    public int getPv() {
        return pv;
    }

    public boolean useObjet(TypePerso target) {
        System.out.println("Avant le heal, : "+target.vie);
        if (target.vie < 100) {
            int result = target.vie + this.pv;
            if (result > 100)
                target.vie = 100;
            else
                target.vie = result;

            System.out.println("Apres le heal, : "+target.vie);
            return true;
        }
        else
            return false;
    }
}
