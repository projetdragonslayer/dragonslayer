package personnage;

/**
 * Created by Romano on 28/05/2016.
 */
public class Voleur extends TypePerso {

    public Voleur(){
        super();
        force = 4;
        dexterite = 8;
        constitution = 4;
        vie = 100;

        inventaire = new Objet[5];
        nbObjet = 0;
        /* ICI Creation des attaques du Voleur ICI */
        Attack attaqueMain = new Attack("Attaque à la dague ","Attaque basique du Voleur, fais perdre 13pv à l'adversaire",-13,0);
        Attack attaque1 = new Attack("Attaque fourbe","inflige 150% de dégât d’arme",-40,0);
        sesCompetences.add(attaqueMain); // Index = 0
        sesCompetences.add(attaque1);
    }

    // Méthode
    /* Ici, on aurait une liste de méthodes qui permettent à un Mage d'attaquer / healer */
    /*public void attaqueMain(TypePerso target){
        this.sesCompetences.get(0).useSkill(target);
    }*/

    public String getJsonSkill() {
        String skillPerso = "";

        for (Skill mySkill : getSesCompetences()) {
            if(mySkill.getTour_A_Attendre() == 0)
                skillPerso += "\"" + mySkill.getSkillName() + "\",";
        }
        return skillPerso.substring(0, skillPerso.lastIndexOf(','));
    }


    public String getJsonObjet() {
        String objetPerso = "";
        Objet myObjet;

        for (int i = 0; i < nbObjet; i++) {
            myObjet =  getInventaire()[i];
            if (!(myObjet==null))
                objetPerso += "\"" + myObjet.getObjetName() + "\",";
        }

        if (nbObjet == 0) {
            return "";
        }
        else
            return objetPerso.substring(0, objetPerso.lastIndexOf(','));
    }

    public static void main(String args[]){
        Voleur patati = new Voleur();
        // leMechant sert de target de test
        Guerrier leMechant = new Guerrier();

        //patati.attaqueMain(leMechant);
    }

}
