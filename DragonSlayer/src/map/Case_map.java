package map;

import personnage.Monstre;
import personnage.Objet;
import personnage.Personnage;
import personnage.Skill;

/**
 * Created by Sandy on 25/04/2016.
 */
public class Case_map {
	protected boolean collision;
	protected Personnage perso;
	private Monstre unMonstre;
	
	public Case_map () {
		collision = false;
	}
	
	public Case_map (boolean collision) {
		this.collision = collision;
	}
	
	public void setCollision(boolean collision) {
		this.collision = collision;
	}
	
	public String setPersonnage(Personnage perso) {
		String returnMsg = null;

		if (collision) 
			return returnMsg;
		else {
			this.perso = perso;
			collision = true;
			returnMsg = "CHAR_MOVED";

			if (!(unMonstre == null)) {
				returnMsg = startBattle();
			}

			return returnMsg;
		}
	}

	public String startBattle() { //Retourne un json à envoyer au client
		String SkillPerso = perso.getTypePerso().getJsonSkill();

		String objetPerso = perso.getTypePerso().getJsonObjet();

		String SkillMonstre = "";
		for (Skill mySkill : unMonstre.getSesCompetences()) {
			SkillMonstre += "\"" + mySkill.getSkillName() + "\",";
		}
		SkillMonstre = SkillMonstre.substring(0, SkillMonstre.lastIndexOf(','));

		/*
		return "{\"serverPacket\":\"startCombat\",\"data\":{\"listeJoueur\":[{\"" + perso.getNomJoueur() + "\":{\"vie\":\"" + perso.getTypePerso().getVie() +
				"\", \"skill\":[" + SkillPerso + "]}}], \"listeMonstre\":[{\"" + unMonstre.getNomMonstre() + "\":{\"vie\":\"" + unMonstre.getVie() + "\", \"skill\":[" + SkillMonstre + "]}}]}}";
		*/
		return "{\"serverPacket\":\"startCombat\",\"data\":{\"" + perso.getNomJoueur() + "\":{\"vie\":\"" + perso.getTypePerso().getVie() +
				"\", \"skill\":[" + SkillPerso + "], \"objet\":[" + objetPerso +"]}, \"Monstre\":{\"nom\":\"" + unMonstre.getNomMonstre() + "\", \"vie\":\"" + unMonstre.getVie() + "\", \"skill\":[" + SkillMonstre + "]}}}";
	}

	public String setMonstre(Monstre unMonstre) {
		if (unMonstre != null) {
			this.unMonstre = unMonstre;
			if (!(perso == null)) {
				//Dans ce cas ci, envoyé un message au client pour dire que le combat commence + démarrer le combat coté serveur
				return startBattle();
			}
		}
		return null;
	}

	public Monstre getMonstre() {
		return unMonstre;
	}

	public void removePersonnage() {
		perso = null;
		collision = false;
	}
}
