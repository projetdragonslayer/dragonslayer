package map;

import java.io.*;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import personnage.Monstre;
import personnage.Personnage;

/**
 * Created by Sandy on 25/04/2016.
 */
public class GameMap {
    private int nbColumn;
    private int nbLine;
    private Case_map mesCase[][];
	private Monstre listeMonstre[];

    public GameMap(int column, int line) {
    	// par d�faut, 38 et 18
        nbColumn = column;
        nbLine = line;
        mesCase = new Case_map[nbLine][nbColumn];
    }
    
    public void loadMapAsset(String map) {
    	int col, line;
    	String jsonRawData = "";
    	String ligne;
		
		try {
			//room = ImageIO.read(getClass().getResource("/room.png"));
			//InputStream in = FileLoadder.class.getResourceAsStream("C:\\Users\\Sandy\\Documents\\dragonslayer\\WebChat\\web\\maps\\" + map + ".json");
			//String filePath = new File("C:\\Users\\Sandy\\Documents\\dragonslayer\\WebChat\\web\\maps\\").getAbsolutePath();


			// FAUT REMPLACER LE CHEMIN QUE J'AI MIS PAR CELUI DU DOSSIER Où YA LES MAPS D'ANTON
			//BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\Sandy\\Documents\\dragonslayer\\WebChat\\web\\maps\\" + map + ".json"));

			//BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\Anton\\OneDrive\\IUT\\Projet DragonSlayer\\DragonSlayer\\web\\maps\\" + map + ".json"));

			//Normalement, sa sa marche
			String filePath = getClass().getResource("").getPath();
			filePath = filePath.replace("/out/artifacts/DragonSlayer_war_exploded/WEB-INF/classes/map", "");
			filePath += "web/maps/"  + map + ".json";
			BufferedReader reader = new BufferedReader(new FileReader(filePath));

			while ((ligne = reader.readLine()) != null) {
				jsonRawData += ligne;
			}
			reader.close();
			
			JSONObject jsonObj = new JSONObject(jsonRawData);
			
	        JSONArray jsonData = jsonObj.getJSONArray("terrain");

			/*if (map.equals("dungeon")) { A prendre en compte si on a différente map à charger
				caseAsansCollision = 1;
				caseBsansCollision = 66;
			}*/


			for (int i = 0; i < jsonData.length(); ++i) {
				JSONArray jsonLine = jsonData.getJSONArray(i);

				for (int j = 0; j < jsonLine.length(); ++j) {
					if (jsonLine.getInt(j) == 1 || jsonLine.getInt(j) == 66)
						mesCase[i][j] = new Case_map(false);
					else
						mesCase[i][j] = new Case_map(true);
				}

			}
		} 
        catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (FileNotFoundException fe) {
			fe.printStackTrace();
		}
		catch (UnsupportedEncodingException ue) {
			ue.printStackTrace();
		}
		catch (IOException e) {
			System.err.format("Exception occurred trying to read '%s'.", "default.txt");
			e.printStackTrace();
		}
    }

	public ArrayList<String> spawnMonstre(Monstre lesMonstres[], int nbMonstre) {
		int ind, c, l, k;
		boolean ok = false;
		listeMonstre = new Monstre[nbMonstre];
		ArrayList<String> jsonToReturn = new ArrayList<String>();
		String tempString;

		for (ind = 0; ind < nbMonstre; ind ++){
			listeMonstre[ind] = new Monstre(lesMonstres[0]);
		}
		mesCase[0][18].setMonstre(lesMonstres[0]); //Temporaise

		for (k = 0; k < nbMonstre; k++) {
			while (!ok) {
				ind = (int) Math.floor(Math.random());
				c = (int) Math.floor(Math.random() * (nbColumn -1));
				l = (int) Math.floor(Math.random() * (nbLine-1));
				if (!(mesCase[l][c].collision && mesCase[l][c].perso == null)) {
					//mesCase[l][c].setMonstre(lesMonstres[ind]); POUR LINSTANT YA QUN SEUL MONSTRE
					tempString = mesCase[l][c].setMonstre(listeMonstre[k]);
					listeMonstre[k].setPos(l,c);

					if (tempString != null) {
						jsonToReturn.add(tempString);
					}
					ok = true;
				}
			}
			ok = false;
		}
		return jsonToReturn;
	}

	public ArrayList<String> moveAllMonstre(){
		int ind, c, l, k, direction, nbDirection;
		boolean ok = false, bas, gauche, droite, haut;
		ArrayList<String> jsonToReturn = new ArrayList<String>();
		String tempString;
		Monstre tempMonstre;
		ArrayList<String> tabPossibilité = new ArrayList<String>();

		//mesCase[0][18].setMonstre(lesMonstres[0]); //Temporaise

		for (k = 0; k < listeMonstre.length; k++) {
			tempMonstre = listeMonstre[k];
			nbDirection = 3;
			tabPossibilité.clear();
			tabPossibilité.add("BAS");
			tabPossibilité.add("GAUCHE");
			tabPossibilité.add("DROITE");
			tabPossibilité.add("HAUT");

			while (!ok && nbDirection >= 0) {
				direction = (int) Math.floor(Math.random() * nbDirection);
				l = tempMonstre.getL();
				c = tempMonstre.getC();

				if (tabPossibilité.get(direction).equals("BAS")) {
					l += 1;
					tabPossibilité.remove("BAS");
				}
				else if (tabPossibilité.get(direction).equals("GAUCHE")) {
					c -= 1;
					tabPossibilité.remove("GAUCHE");
				}
				else if (tabPossibilité.get(direction).equals("DROITE")) {
					c += 1;
					tabPossibilité.remove("DROITE");
				}
				else if (tabPossibilité.get(direction).equals("HAUT")) {
					l -=1;
					tabPossibilité.remove("HAUT");
				}
				nbDirection -= 1;

				if (mesCase[l][c].collision && !(mesCase[l][c].perso == null && mesCase[l][c].getMonstre() == null)) {
					//mesCase[l][c].setMonstre(lesMonstres[ind]); POUR LINSTANT YA QUN SEUL MONSTRE
					if ((tempString = mesCase[l][c].setMonstre(listeMonstre[k])) != null) {
						jsonToReturn.add(tempString);
					}
					ok = true;
				}
				else if (!(mesCase[l][c].collision && mesCase[l][c].getMonstre() == null)) {
					if ((tempString = mesCase[l][c].setMonstre(listeMonstre[k])) != null) {
						jsonToReturn.add(tempString);
					}
					ok = true;
				}
			}
			ok = false;
		}
		return jsonToReturn;
	}


    public String setPerso(Personnage perso, int column, int line) {
		String tempMsg;
		tempMsg = mesCase[line][column].setPersonnage(perso);

    	if (!(tempMsg == null)) {
			perso.setPos(column, line);
		}

		return tempMsg;

    }

	public void removePerso(int column, int line) {
		mesCase[line][column].removePersonnage();
	}

    public Case_map getCaseByIndex (int column, int line) {
    	return mesCase[line][column];
    }
}
