import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import jdk.nashorn.internal.parser.JSONParser;
import model.ModelServeur;
import org.json.JSONException;
import org.json.*;
import com.google.gson.*;
import personnage.Personnage;


@ServerEndpoint("/chatroomServer")

public class ChatRoomServerEndPoint {
    static Set<Session> chatroomUsers = Collections.synchronizedSet(new HashSet<Session>());
    private static ModelServeur mServeur = new ModelServeur(chatroomUsers);

    @OnOpen
    public void handleOpen(Session userSession) throws IOException {
        System.out.println("On est dans serveur: Open\n");
        chatroomUsers.add(userSession);
        for (Session chatroomUser : chatroomUsers) {
            (chatroomUser).getBasicRemote().sendText(buildJsonUsersData());
        }
    }

    @OnMessage
    public void handleMessage(String message, Session userSession) throws IOException {
        System.out.println("On est dans serveur: handleMessage\n");
        try {

            String username = (String) userSession.getUserProperties().get("username");
            Iterator<Session> iterator = chatroomUsers.iterator();
            String jsonToSend;

            if(username==null) {
                if (mServeur.getChar(message) == null) {
                    userSession.getUserProperties().put("username", message);
                    mServeur.createChar(message);
                    userSession.getBasicRemote().sendText(buildJsonMessageData("System", "you are now connected as " + message));
                    while (iterator.hasNext())
                        (iterator.next()).getBasicRemote().sendText(buildJsonUsersData());

                    jsonToSend = mServeur.getAllJSONCharCoord();
                    userSession.getBasicRemote().sendText(jsonToSend);
                }
            }
            else {
                JSONObject jsonObj = new JSONObject(message);
                String jsonTypeMsg = (String) jsonObj.get("clientPacket");

                //Si le packet reçu est de tyoe "charGoTo" qui dit si un personnage peut se déplacer
                if (jsonTypeMsg.equals("charGoTo")) {
                    JSONObject jsonData = jsonObj.getJSONObject("data");
                    int direction = Integer.parseInt(jsonData.getString("direction"));
                    String returnMsg;

                    returnMsg = mServeur.moveChar(username, direction);

                     if (!(returnMsg == null)) {
                        //On créer le json majMap pour les clients
                        jsonToSend = mServeur.getJSONCharCoord(username);

                        //On met a jour l'affichage de tout les clients
                        while (iterator.hasNext())
                            iterator.next().getBasicRemote().sendText(jsonToSend);

                        if (!(returnMsg.equals("CHAR_MOVED"))) {
                            userSession.getBasicRemote().sendText(returnMsg);
                        }
                    }
                }
                else if (jsonTypeMsg.equals("chatMsg")) { //Pour les messages du chat
                    jsonToSend = "{\"serverPacket\":\"chatMsg\",\"data\":{\"" + username + "\":\"" + jsonObj.getString("data") + "\"}}";

                    while (iterator.hasNext()) {
                        iterator.next().getBasicRemote().sendText(jsonToSend);
                    }
                }
                else if (jsonTypeMsg.equals("combatMsg")) {
                    JSONObject jsonData = jsonObj.getJSONObject("data");
                    JSONObject temp = jsonData.getJSONObject("surnom");
                    String action = temp.getString("action");
                    Personnage tempPerso = mServeur.getChar(username);

                    //if (!(action.equals("defense") || action.equals("escape"))) {
                    jsonToSend = tempPerso.getCombat().doAction(action, username);

                    userSession.getBasicRemote().sendText(jsonToSend);

                    if (tempPerso.getCombat().finCombat()) {
                        jsonToSend = tempPerso.getCombat().getJSONfinCombat();
                        tempPerso.getCombat().getLeMonstre().setVie(40);
                        mServeur.getListeDeCombat().remove(tempPerso.getCombat());
                        tempPerso.setCombat(null);
                        userSession.getBasicRemote().sendText(jsonToSend);
                    }
                }
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @OnClose
    public void handleClose(Session userSession) throws IOException {
        chatroomUsers.remove(userSession);
        mServeur.disconnectChar((String) userSession.getUserProperties().get("username"));

        for (Session chatroomUser : chatroomUsers) {
            (chatroomUser).getBasicRemote().sendText(buildJsonUsersData());
        }
    }

    private void sendJSONToAll(String myJSON) throws IOException {
        Iterator<Session> iterator = chatroomUsers.iterator();
        while (iterator.hasNext())
            iterator.next().getBasicRemote().sendText(myJSON);
    }

    private String buildJsonUsersData() throws IOException {
        System.out.println("On appelle buildJsonUsersData\n");
        Iterator<String> iterator = getUserNames().iterator();
        JsonArrayBuilder jsonArrayBuilder = Json.createArrayBuilder();
        while (iterator.hasNext()) {
            jsonArrayBuilder.add(iterator.next());
        }
        return Json.createObjectBuilder().add("users", jsonArrayBuilder).build().toString();
    }

    private String buildJsonMessageData(String username, String message) {
        System.out.println("On appelle buildJsonMessageData\n");
        JsonObject jsonObject = Json.createObjectBuilder().add("message", username+": "+message).build();
        StringWriter stringWriter = new StringWriter();

        try	(JsonWriter jsonWriter = Json.createWriter(stringWriter)) {
            jsonWriter.write(jsonObject);
        }
        return stringWriter.toString();
    }

    private Set<String> getUserNames() {
        HashSet<String> returnSet = new HashSet<>();
        for (Session chatroomUser : chatroomUsers) {
            returnSet.add(chatroomUser.getUserProperties().get("username").toString());
        }
        return returnSet;
    }
}
