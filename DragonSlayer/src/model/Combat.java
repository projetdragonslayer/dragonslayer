package model;

import personnage.*;

import java.text.DecimalFormat;

/**
 * Created by Sandy on 27/05/2016.
 */
public class Combat {
    private Personnage listeJoueur[];
    private Monstre listeMonstre[];
    private boolean tourMonstre;
    private boolean tourJoueur;
    private ModelServeur mServeur;

    public Combat(Personnage lesJoueurs[], Monstre lesMonstres[], ModelServeur mServeur) {
        listeJoueur = new Personnage[3];
        listeMonstre = new Monstre[3];
        tourMonstre = false;
        tourJoueur = true;
        this.mServeur = mServeur;

        if (lesJoueurs.length <= 3 && lesMonstres.length <= 3) {
            int i = 0;
            for (Personnage joueur : lesJoueurs) {
                if (joueur != null) {
                    listeJoueur[i] = joueur;
                    i++;
                }
            }
            i = 0;
            for (Monstre monstre : lesMonstres) {
                if (monstre != null) {
                    listeMonstre[i] = monstre;
                    i++;
                }
            }
        }
        else {
            System.out.println("Erreur: nombre de monstre ou de joueurs incorrecte. Max = 4 (0 à 3 pour un tableau)");
        }
    }

    public String doAction(String action, String origin) {
        Personnage tempPerso;
        Personnage tempPerso2;
        Monstre tempMonstre = listeMonstre[0];
        String returnMsg = "";

        tempPerso = listeJoueur[0];

        for (Skill skill : tempPerso.getTypePerso().getSesCompetences()) {
            skill.tourUp();
        }

        if (action.equals("defense")) {

        }
        else if (action.equals("escape")) {

        }
        else {
            Skill tempSkill = tempPerso.getTypePerso().getSkillByName(action);

            if (tempSkill == null) {
                Objet tempObjet = tempPerso.getTypePerso().getObjetByName(action);
                returnMsg = tempPerso.getNomJoueur() + " à utiliser l'objet " + tempObjet.getObjetName();

                if (tempObjet.useObjet(tempPerso.getTypePerso())) {
                    returnMsg += " avec succès! +" + tempObjet.getPv() + " pv";
                    tempPerso.getTypePerso().removeObjet(tempObjet);
                }
                else {
                    returnMsg += " avec echec!";
                }
            }
            else {
                Attack tempAttack;
                Buff tempBuff;
                Heal tempHeal;

                if (tempSkill.attack) {
                    tempAttack = (Attack) tempSkill;
                    double chance = tempPerso.getTypePerso().getAttribut("dexterite");
                    double rand = Math.random();
                    double result = (((rand * (chance/ 5.0))/chance)/2.0)*10.0;
                    /*DecimalFormat f = new DecimalFormat("##.00");
                    String temp = "0"+f.format(result);
                    result = Double.parseDouble(temp);*/
                    int randVal = (int)Math.round(result);

                    returnMsg = tempPerso.getNomJoueur() + " à utiliser la compétence " + tempAttack.getSkillName() + " sur " + tempMonstre.getNomMonstre();

                    if (randVal == 1) {
                        if (tempAttack.useSkill(tempMonstre))
                            returnMsg += " avec succès! " + tempAttack.hp + " pv";
                        else
                            returnMsg += " avec echec!";
                    }
                    else {
                        returnMsg += " avec echec!";
                    }
                }
                else if (tempSkill.buff) {
                    //tempPerso2 = getJoueur(target);
                    tempPerso2 = tempPerso;
                    tempBuff = (Buff) tempSkill;
                    tempBuff.useSkill(tempPerso2.getTypePerso());
                    returnMsg = tempPerso.getNomJoueur() + " à utiliser la compétence " + tempBuff.getSkillName() + " sur " + tempPerso2.getNomJoueur() +
                            " avec succès! " + tempBuff.nomCaracABooster + tempBuff.qteCaracABooster + tempBuff.nbtours;

                }
                else if (tempSkill.heal) {
                    //tempPerso2 = getJoueur(target);
                    tempPerso2 = tempPerso;
                    tempHeal = (Heal) tempSkill;
                    tempHeal.useSkill(tempPerso2.getTypePerso());
                    returnMsg = tempPerso.getNomJoueur() + " à utiliser la compétence " + tempHeal.getSkillName() + " sur " + tempPerso2.getNomJoueur() +
                            " avec succès! +" + tempHeal.getHp() + "pv";

                }
            }
        }

        returnMsg = "\"serverLogJoueur\":\"" + returnMsg + "\"";
        String returnMonstreMsg = null;

        switchTour();

        if (tempMonstre.getVie() == 0) {
            returnMonstreMsg = "\"serverLogMonstre\":\"" + tempMonstre.getNomMonstre() + " n'a pas pu attaqué car il est mort\"";
        }
        else {
            returnMonstreMsg = doMonstreAction();
        }

        returnMsg = "{\"serverPacket\":\"combatMsg\",\"data\":{" + returnMsg + ",\"vieJoueur\":\"" + tempPerso.getTypePerso().getVie() +
                "\"," + returnMonstreMsg + ",\"vieMonstre\":\"" + tempMonstre.getVie() + "\",\"skill\":[" + tempPerso.getTypePerso().getJsonSkill() +
                "], \"objet\":[" + tempPerso.getTypePerso().getJsonObjet() + "]}}";

        switchTour();

        return returnMsg;
    }

    public String doMonstreAction() {
        Monstre tempMonstre = listeMonstre[0];
        Personnage tempPerso = listeJoueur[0];
        String returnMsg;

        Attack tempAttack = (Attack) tempMonstre.getSesCompetences().get(0);
        double chance = tempMonstre.getAttribut("dexterite");
        double rand = Math.random();
        double result = (((rand * (chance/ 5.0))/chance)/2.0)*10.0;
        /*DecimalFormat f = new DecimalFormat("##.00");
        result = Double.parseDouble(f.format(result));*/
        int randVal = (int)Math.round(result);
        returnMsg = tempMonstre.getNomMonstre() + " attaque " + tempPerso.getNomJoueur();

        if (randVal == 1) {
            if (tempAttack.useSkill(tempPerso.getTypePerso()))
                returnMsg += " avec succès! " + tempAttack.hp + " pv";
            else
                returnMsg += " avec echec!";
        }
        else {
            returnMsg += " avec echec!";
        }

        return "\"serverLogMonstre\":\"" + returnMsg + "\"";
    }

    public boolean finCombat() {
        if (listeJoueur[0].getTypePerso().getVie() == 0 || listeMonstre[0].getVie() == 0)
            return true;
        else
            return false;
    }

    public String getJSONfinCombat() {
        Personnage tempPerso = listeJoueur[0];
        Monstre tempMonstre = listeMonstre[0];
        String returnMsgJoueur = "";
        String returnMsgMonstre = "";
        String returnMsgObjetDrop = "";

        if (tempPerso.getTypePerso().getVie() == 0) {
            returnMsgJoueur = "\"serverLogJoueur\":\"" + tempPerso.getNomJoueur() + " a perdu contre " + tempMonstre.getNomMonstre() + ". -" + tempMonstre.getXpDrop() + "xp\"";
            returnMsgMonstre = "\"serverLogMonstre\":\"" + tempMonstre.getNomMonstre() + " a gagné contre " + tempPerso.getNomJoueur() + ".\"";
            tempPerso.addXp(-tempMonstre.getXpDrop());
            returnMsgObjetDrop = "\"objetDrop\":\"aucun\"";
            tempPerso.getTypePerso().setVie(100);
        }
        else if (tempMonstre.getVie() == 0){
            returnMsgJoueur = "\"serverLogJoueur\":\"" + tempPerso.getNomJoueur() + " a gagné contre " + tempMonstre.getNomMonstre() + ". +" + tempMonstre.getXpDrop() + "xp\"";
            returnMsgMonstre = "\"serverLogMonstre\":\"" + tempMonstre.getNomMonstre() + " a perdu contre " + tempPerso.getNomJoueur() + ".\"";
            tempPerso.addXp(tempMonstre.getXpDrop());

            Objet tempObjet = mServeur.getRandomObjet();
            tempPerso.getTypePerso().addObjet(tempObjet);

            returnMsgObjetDrop = "\"objetDrop\":\"" + tempObjet.getObjetName() + "\"";
        }

        return "{\"serverPacket\":\"endCombat\",\"data\":{" + returnMsgJoueur + "," + returnMsgMonstre + "," + returnMsgObjetDrop + "}}";
    }

    private Personnage getJoueur(String nomJoueur) {
        int i = 0;
        boolean ok = false;

        while (i < listeJoueur.length && !ok) {
            if ((listeJoueur[i] == null)) {
                ok = listeJoueur[i].getNomJoueur().equals(nomJoueur);
            }
            i++;
        }

        if (ok)
            return listeJoueur[i-1];
        else
            return null;
    }

    public Monstre getLeMonstre() {
        return listeMonstre[0];
    }

    private Monstre getMonstre(String nomMonstre) {
        int i = 0;
        boolean ok = false;

        while (i < listeMonstre.length && !ok) {
            if ((listeMonstre[i] == null)) {
                ok = listeMonstre[i].getNomMonstre().equals(nomMonstre);
            }
            i++;
        }

        if (ok)
            return listeMonstre[i-1];
        else
            return null;
    }

    private void switchTour() {
        if (tourMonstre) {
            tourMonstre = false;
            tourJoueur = true;
        }
        else {
            tourMonstre = true;
            tourJoueur = false;
        }
    }
}
