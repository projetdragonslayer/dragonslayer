package model;

import java.sql.*;

/**
 * Created by Anton on 01/06/2016.
 */
public class BDDConnexion {
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;

    public void seConnecter() {

        // This will load the MySQL driver, each DB has its own driver
        try {
            Class.forName("com.mysql.jdbc.Driver");

            // Setup the connection with the DB

            connect = DriverManager.getConnection("jdbc:mysql://localhost/dragonslayer?" + "user=altairr&password=dragon");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getClassePerso(String userName) {
        try {
            // Statements allow to issue SQL queries to the database
            statement = connect.createStatement();
            // Result set get the result of the SQL query
            userName = "\'" + userName + "\'";
            resultSet = statement.executeQuery("select * from utilisateurs where nickname = " + userName);

            resultSet.next();
            return resultSet.getString(3);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void seDeconnecter() {
        try {
           connect.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String argv[]) throws SQLException, ClassNotFoundException {
        BDDConnexion test = new BDDConnexion();
        test.seConnecter();
        test.getClassePerso("sandy");
    }
}
