package model;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Sandy on 30/05/2016.
 */
public class ActionRepete {
    Timer t;
    ModelServeur mServer;

    public ActionRepete(ModelServeur mServer) {
        this.mServer = mServer;
        t = new Timer();
        t.schedule(new MonAction(), 0, 1*1000);
    }

    class MonAction extends TimerTask {
        //int nbrRepetitions = 3;

        public void run() {
            ArrayList<String> tempMsg = mServer.getMap1().moveAllMonstre();
            mServer.sendCombatJson(tempMsg);
        }
    }
}
