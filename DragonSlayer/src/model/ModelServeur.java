package model;

import map.Case_map;
import map.GameMap;
import personnage.Attack;
import personnage.Monstre;
import personnage.Objet;
import personnage.Personnage;

import javax.websocket.Session;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by Sandy on 23/04/2016.
 */
public class ModelServeur {
	public final static int BAS = 0;
	public final static int GAUCHE = 1;
	public final static int DROITE = 2;
	public final static int HAUT = 3;
	public final static int MAX_JOUEUR = 24;
	
	//public final static Case_map assetMap1[][] = new Case_map[][] {
	Set<Session> chatroomUsers;
	
	protected static Personnage joueur[];
	protected int nbJoueur;
	protected static GameMap map1;

	private static Monstre monstres1[];
	private static Monstre monstres2[];

	ArrayList<Objet> listeObjet;
	ArrayList<Combat> listeDeCombat;

	BDDConnexion connexionMySQL;

    public ModelServeur(Set<Session> chatroomUsers) {
		listeDeCombat = new ArrayList<Combat>();
		listeObjet = new ArrayList<Objet>();
		listeObjet.add(new Objet("Potion de vie", "on sen fou", 15));
		listeObjet.add(new Objet("Grande Potion de vie", "on sen fou", 30));

		ArrayList<String> tempArray;
		this.chatroomUsers = chatroomUsers;
    	map1 = new GameMap(38, 18);
    	map1.loadMapAsset("dungeon");
    	joueur = new Personnage[MAX_JOUEUR];
		nbJoueur = 0;

		//On instancie les monstres de notre jeux

		monstres1 = new Monstre[2]; // Gobelins et Orcs
		//Creation d'un Gobelins
		Monstre gobelins = new Monstre("Gobelins", 1, 2, 3, 40, 20);
		Attack attaqueMain = new Attack("Attaque","Attaque basique du gobelins, fais perdre 6pv à l'adversaire",-18,0);
		gobelins.getSesCompetences().add(attaqueMain);
		monstres1[0] = gobelins;
		monstres2 = new Monstre[2]; // Héros déchu et Serviteur du dragon


/*if ((tempArray = map1.spawnMonstre(monstres1, 5)) != null) { NE SE FAIT PAS AU LANCEMENT DU SERVEUR
			sendCombatJson(tempArray);
		}*/

		tempArray = map1.spawnMonstre(monstres1, 5);

        //Instancier les données depuis la base de données


    	//on instancie le perso admin ou celui de teste je pense
    	//joueur[nbJoueur] = new Personnage("Admin");
		//nbJoueur ++;
    	
    	//on place le joueur � un emplacement par d�faut (x=18, y=1) sur la map
    	//map1.setPerso(joueur[0], 18, 1);
    }

	public GameMap getMap1() {
		return map1;
	}

	public boolean createChar (String charName) {
		connexionMySQL = new BDDConnexion();
		connexionMySQL.seConnecter();

		if (getChar(charName) == null) {
			if (nbJoueur < MAX_JOUEUR) {
				joueur[nbJoueur] = new Personnage(charName, connexionMySQL.getClassePerso(charName), 1, 10);
				joueur[nbJoueur].getTypePerso().addObjet(listeObjet.get(0));
				joueur[nbJoueur].getTypePerso().addObjet(listeObjet.get(1));
				nbJoueur++;
				connexionMySQL.seDeconnecter();
				return true;
			}
			else
				connexionMySQL.seDeconnecter();
				return false;

		}
		else
			connexionMySQL.seDeconnecter();
			return false;
	}

	public boolean disconnectChar (String charName) {
		int i = 0, x = 0, y = 0;
		boolean trouver = false;


		if (!(charName == null)) {
			i = getCharIndice(charName);
			if (!(i == -9999)) {
				x = joueur[i].getX();
				y = joueur[i].getY();
				map1.removePerso(x, y);
				joueur[i] = joueur[nbJoueur];
				nbJoueur --;
				return true;
			}
		}
		return false;
	}

	public Objet getRandomObjet() {
		int ind = (int) Math.floor(Math.random() * (listeObjet.size() -1)); // A VERIFIER

		return listeObjet.get(ind);
	}

	public void sendCombatJson(ArrayList<String> listOfJson) {
		//(String) userSession.getUserProperties().get("username");
		String username;
		Iterator<Session> iterator = chatroomUsers.iterator();

		while (iterator.hasNext()) {
			Session tempSession = iterator.next();
			username = (String) tempSession.getUserProperties().get("username");
			for (String val : listOfJson) {
				if (val.contains(username))
					try {
						tempSession.getBasicRemote().sendText(val);
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
		}
	}

	public int getCharIndice(String charName) {
		int i = 0;
		boolean trouver = false;

		while (i < nbJoueur && !trouver) {
			if (!(joueur[i] == null)) {
				trouver = joueur[i].getNomJoueur().equals(charName);
				i++;
			}
		}

		if (trouver)
			return i-1;
		else
			return -9999;
	}

    public Personnage getChar(String charName) {
    	int i = 0;
    	boolean trouver = false;
    	
        while (i < nbJoueur && !trouver) {
			if (!(joueur[i] == null)) {
				trouver = joueur[i].getNomJoueur().equals(charName);
				i++;
			}
        }
        
        if (trouver)
        	return joueur[i-1];
        else
        	return null;
    }
    
    //mServeur.moveChar(mServeur.getzCharName(...), jsonObj.get("x"), jsonObj.get("y"));
    public String moveChar(String charName, int direction) {
        String returnMsg = null;
        int column = 0, line = 0;
        int oldC = 0, oldL = 0;
        Personnage tempPerso;
        
        if ((tempPerso = getChar(charName)) == null) {
            System.out.println("Erreur moveChar: perso innexistant");
            return null;
        }
        else {
        	oldC = tempPerso.getX();
        	oldL = tempPerso.getY();
			column = oldC;
			line = oldL;
        	
        	switch(direction) {
		        case BAS :
		            line = line + 1;
					tempPerso.setDirection(BAS);
		            break;
		        case GAUCHE :
		            column = column - 1;
					tempPerso.setDirection(GAUCHE);
		            break;
		        case DROITE :
		        	column = column + 1;
					tempPerso.setDirection(DROITE);
		            break;
		        case HAUT :
		        	line = line - 1;
					tempPerso.setDirection(HAUT);
		            break;
        	}
			if (!(line == 1 && column == 18)) {
				returnMsg = map1.setPerso(tempPerso, column, line);
				if (!(returnMsg == null)) {
					map1.removePerso(oldC, oldL);
					if (!(returnMsg.equals("CHAR_MOVED"))) {
						Personnage tempTabPerso [] = new Personnage[1];
						Monstre tempTabMonstre [] = new Monstre[1];
						tempTabPerso[0] = tempPerso;
						tempTabMonstre[0] = map1.getCaseByIndex(column,line).getMonstre();

						listeDeCombat.add(new Combat(tempTabPerso, tempTabMonstre, this)); //ON DEMARRE UN COMBAT
						tempPerso.setCombat(listeDeCombat.get(listeDeCombat.size()-1));
					}
				}
			}
        }
        
        return returnMsg;
    }

	public ArrayList<Combat> getListeDeCombat() {
		return listeDeCombat;
	}

	//"{\"phonetype\":\"N95\",\"cat\":\"WP\"}"
	public String getJSONCharCoord(String nomPerso) {
		String charListeJSON = null;
		Personnage tempChar;
		boolean trouver = false;
		int i = 0;

		tempChar = getChar(nomPerso);
		if (!(tempChar == null)) {
			trouver = true;
			charListeJSON = "\"" + tempChar.getNomJoueur() + "\" : {\"direction\":\"" + tempChar.getDirection() + "\", \"x\":\"" + tempChar.getX() + "\",\"y\":\"" + tempChar.getY() + "\"}";
		}

		return "{\"serverPacket\":\"majMap\",\"data\":{" + charListeJSON + "}}";
	}

	public String getAllJSONCharCoord() { //Pour mapLoad, lorsqu'un joueur se connecte
		String charListeJSON = null;

		for (Personnage tempChar : joueur) {
			if (!(tempChar == null)) {
				if (!(charListeJSON == null)) {
					charListeJSON += ",";
				}
				else
					charListeJSON = "";
				//"admin": {"direction":"0", "x": "12", "y": "14"},
				charListeJSON += "\"" + tempChar.getNomJoueur() + "\" : {\"direction\":\"" + tempChar.getDirection() + "\", \"x\":\"" + tempChar.getX() + "\",\"y\":\"" + tempChar.getY() + "\"}";
			}
		}

		return "{\"serverPacket\":\"loadMap\",\"data\":{" + charListeJSON + "}}";
	}
}

