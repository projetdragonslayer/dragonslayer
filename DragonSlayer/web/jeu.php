<?php
	session_start();
	$nickname = $_SESSION['nickname'];

	if($nickname==""){
		header('Location: index.html');
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr" >
<head>
	<title>DragonSlayer beta v1.0</title>
	<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
</head>
<body>
	<div class="menu">
		<ul>
			<li>Jeu</li>
			<li><a href="profil.php">Profil</a></li>
			<li><a href="story.php">Histoire</a></li>
			<li><a href="aide.php">Aide</a></li>
			<li><a href="about.php">A propos</a></li>
		</ul>
	</div>
	<div class="affichage">
		<div class="game">
			<canvas id="canvas">Votre navigateur ne supporte pas HTML5, veuillez le mettre à jour pour jouer.</canvas>
			<script>var nickname = "<?php echo $nickname; ?>"</script>
			<script type="text/javascript" src="js/websocket.js"></script>
			<script type="text/javascript" src="js/oXHR.js"></script>
            <script type="text/javascript" src="js/tileset.js"></script>
            <script type="text/javascript" src="js/map.js"></script>
			<script type="text/javascript" src="js/personnage.js"></script>
            <script type="text/javascript" src="js/game.js"></script>
		</div>
		<div class="chat">
			<label for="messagesTextArea"></label><textarea id="messagesTextArea" readonly="readonly" rows="35" cols="45"></textarea>
			<label for="usersTextArea"></label><textarea id="usersTextArea" readonly="readonly" rows="35" cols="20"></textarea><br/>
			<label for="messageText"></label><input type="text" id="messageText" size="50"/>
			<input type="button" value="Send" onclick="sendMessage();"/>
		</div>
	</div>
</body>

</html>