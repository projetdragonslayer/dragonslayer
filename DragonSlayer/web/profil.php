<?php
	session_start();
	$nickname = $_SESSION['nickname'];

	include('connexion.php');

	$infoUtilisateurs = $bdd->query("select nickname, personnage, puissance, dexterite, constitution, intelligence, perception from utilisateurs natural join personnage where nickname = '$nickname'");
	$donnees = $infoUtilisateurs->fetch();
	$typePerso = $donnees["personnage"];

	if($typePerso == "guerrier") {
		$url="images/guerrier.jpg";
	}
	elseif ($typePerso == "mage") {
		$url="images/mage.jpg";
	}
	else {
		$url = "images/voleur.jpg";
	}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr" >
<head>
	<title>DragonSlayer beta v1.0</title>
	<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
</head>
<body>
	<div class="menu">
		<ul>
			<li><a href="jeu.php">Jeu</a></li>
			<li>Profil</li>
			<li><a href="story.php">Histoire</a></li>
			<li><a href="help.php">Aide</a></li>
			<li><a href="about.php">A propos</a></li>
		</ul>
	</div>
	<div class="profil">
		<p>
			<h1>Surnom: <?php echo $nickname; ?></h1>
			<h1>Personnage: <?php echo $donnees["personnage"]; ?></h1>
			<h2>Attribut force: <?php echo $donnees["puissance"] ?></h2>
			<h2>Attribut dexterité: <?php echo $donnees["dexterite"] ?></h2>
			<h2>Attribut constitution: <?php echo $donnees["constitution"] ?></h2>
			<h2>Attribut intelligence: <?php echo $donnees["intelligence"] ?></h2>
			<h2>Attribut perception: <?php echo $donnees["perception"] ?></h2>
		</p>
	</div>
	<div class="imageProfil">
		<img src="<?php echo $url?>" height="325" width="200px" alt="image type perso" />
	</div>
	
</body>

</html>