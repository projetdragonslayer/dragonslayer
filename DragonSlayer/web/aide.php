<?php
session_start();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr" >
<head>
    <title>DragonSlayer beta v1.0</title>
    <link rel="stylesheet" type="text/css" href="css/stylesheet.css">
</head>
<body>
<div class="menu">
    <ul>
        <li><a href="jeu.php">Jeu</a></li>
        <li><a href="profil.php">Profil</a></li>
        <li><a href="story.php">Histoire</a></li>
        <li>Aide</li>
        <li><a href="about.php">A propos</a></li>
    </ul>
</div>
<div class="aide">
    <p>
        Comment jouer ?<br/>

        Si vous n'avez pas de compte, créez-vous un compte, afin d'obtenir un login et un mot de passe.
        Si vous avez un compte, connectez-vous avec votre login/mot de passe<br/>
    </p>

    <p> Cliquez sur la page "Jeu" afin de commencer à jouer.<br/>

        Quelles sont les interactions possibles :
        Le joueur peut se déplacer à l'ade des touches fléchées du clavier.
        Il peut discuter avec les autres joueurs à travers le chat.

        Quand le joueur renconre un monstre, son personnage passe en mode combat. Il peut :<br/>
        - attaquer
        - se soignr
        - utiliser un objet
        - fuir

        Si vous tuez votre ennemi, vous récupererez un objet, et gagnerez des points d'expérience.
        Si vous vous faites tuer, vos perdrez des points d'expériences.</br/>

        Bon jeu !
    </p>
</div>
</body>
</html>
