<?php
	session_start();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr" >
<head>
	<title>DragonSlayer beta v1.0</title>
	<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
</head>
<body>
	<div class="menu">
		<ul>
			<li><a href="jeu.php">Jeu</a></li>
			<li><a href="profil.php">Profil</a></li>
			<li>Histoire</li>
			<li><a href="aide.php">Aide</a></li>
			<li><a href="about.php">A propos</a></li>
		</ul>
	</div>
	<div class="story">
		<p>Dans une vaste contrée lointaine et médiévale, un village est attaqué et dévasté tous les 10 ans par un Dragon nommé Baalkane. Aujourd’hui, la date approche et il se trouve que vous, héro, avait découvert une carte menant à la cachette du Dragon.
		Votre quête commencera dès que vous entrerez dans sa cachette. Mais attention, une fois à l’intérieur vous serez enfermé et seul contre tous… Ou pas ! Vous n’êtes pas le seul héros a vous mesurez contre le grand Baalkane. Joignez-vous à ses héros pour accomplir votre quête ou terrassez les et devenez le plus fort de tous !
		Si vous réussissez à terrasser Baalkane, vous aurez sauvé votre village !!! … Mais pas le monde !
		Car Baalkane préparera sa revanche depuis les enfers et tentera de devenir plus puissant que jamais, à tel point qu’il pourra détruire le monde !! Cette fois-ci vous devrez agir avec une équipe de 4 héros et partir dans les enfers pour sauver le monde !
		A présent, que votre quête commence !
		</p>
	</div>
</body>
</html>