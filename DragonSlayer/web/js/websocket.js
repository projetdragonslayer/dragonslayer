/**
 * Created by Anton on 06/05/2016.
 */

var wsUri = "ws://25.40.122.216:8080/DragonSlayer_war_exploded/chatroomServer";
var websocket = new WebSocket(wsUri);
var surnom = nickname;
var messageCombat;
var chat=true;

    console.log("Connecting status: " + websocket.readyState);


websocket.onopen = function sayHi() {
    console.log("Hello, connection established!");
    console.log("Connecting status: " + websocket.readyState);

    websocket.send(nickname);
    joueur = new Personnage(nickname, "test.png", 18, 1, DIRECTION.BAS);
    map.addPersonnage(joueur);
    document.getElementById('messagesTextArea').value += "Bienvenue sur DragonSlayer, " + nickname + "\n";
    document.getElementById('usersTextArea').value += nickname + "\n";
};

websocket.onmessage = function processMessage(message) {
    console.log("On recoit du serveur");
    var jsonData = JSON.parse(message.data);
    if(jsonData.serverPacket==="loadMap") {
        var k=0;
        console.log("On est dans serverPacket - loadMap");
        for(var user in jsonData.data) {
            console.log(jsonData.data[user]);
            if (user !== surnom) {
                player = new Personnage(user, "test.png", jsonData.data[user].x, jsonData.data[user].y, jsonData.data[user].direction);
                lesJoueurs.unshift(k, player);
                map.addPersonnage(player);
                document.getElementById('usersTextArea').value += user + "\n";
            }
            k++;
        }
    }
    if(jsonData.serverPacket==="majMap") {
        var userExist = false;
        console.log("On est dans serverPacket - majMap");
        for(var user in jsonData.data) {
            console.log(jsonData.data[user]);
            if(user===surnom) {
                joueur.deplacer(jsonData.data[user].direction, jsonData.data[user].x, jsonData.data[user].y)
            }
            else {
                for(var i = 0;i<lesJoueurs.length;i++) {
                    if(lesJoueurs[i].nickname === user) {
                        lesJoueurs[i].deplacer(jsonData.data[user].direction, jsonData.data[user].x, jsonData.data[user].y);
                        userExist=true;
                    }
                }
                if(!userExist) {
                    player = new Personnage(user, "test.png", jsonData.data[user].x, jsonData.data[user].y, jsonData.data[user].direction);
                    lesJoueurs.push(player);
                    map.addPersonnage(player);
                    document.getElementById('usersTextArea').value += user + "\n";
                }
            }
        }
    }
    if (jsonData.serverPacket==="chatMsg") {
        console.log("On recoit du serveur chatMsg:");
        var chatBox = document.getElementById('messagesTextArea');
        for(var utilisateur in jsonData.data) {
            var nickname = utilisateur;
            var message = jsonData.data[utilisateur];
            chatBox.value += "\n" + nickname + " : " + message;
        }
    }
    
    if(jsonData.serverPacket==="startCombat") {
        chat = false;
        console.log("On recoit du serveur startCombat:");
        clearInterval(dessinMap);

        // On doit disable keydowns on canvas mais côté serveur!
        keydown = false;

        var heroe = new Image();
        var monster = new Image();

        heroe.onload = function() {
            // switch selon le type de l'héro
            ctx.drawImage(heroe, 0, 0);
        };
        monster.onload = function () {
            ctx.drawImage(monster, 525, -50);
        };
        heroe.src = "images/Heroe.svg";
        monster.src = "images/Monstre.svg";

        var healthUser = jsonData.data[surnom].vie;
        attackUser = "";
        for(var skill in jsonData.data[surnom].skill) {
            attackUser+=jsonData.data[surnom].skill[skill] + "\n";
        }
        objetUser = "";
        for(var objet in jsonData.data[surnom].objet) {
            objetUser+=jsonData.data[surnom].objet[objet] + "\n";
        }

        var nomMonstre = jsonData.data["Monstre"].nom;
        var healthMonstre = jsonData.data["Monstre"].vie;
        var attackMonstre = "";
        for(var skill in jsonData.data["Monstre"].skill) {
            attackMonstre+=jsonData.data["Monstre"].skill[skill] + "\n";
        }

        var combatBox = document.getElementById('messagesTextArea');
        messageCombat = "Vous entrez en mode combat: \n\nSanté: " + healthUser + "\nActions disponibles: \n" + attackUser + "\tObjets disponibles:\n" + objetUser  + "\n\n";
        messageCombat+="Ennemie: " + nomMonstre + " avec " + healthMonstre + " santé et " + attackMonstre + " attack\n\n";
        messageCombat+="Que voulez vous faire?\n";

        combatBox.value = messageCombat;
    }
    if(jsonData.serverPacket==="combatMsg") {
        console.log("On reçois combatMsg");
        attackUser = "";
        objetUser = "";
        var combatBox = document.getElementById('messagesTextArea');

        for(var skill in jsonData.data["skill"]) {
            attackUser+=jsonData.data["skill"][skill] + "\n";
        }
        objetUser = "";
        for(var objet in jsonData.data["objet"]) {
            objetUser+=jsonData.data["objet"][objet] + "\n";
        }

        messageCombat = jsonData.data["serverLogJoueur"] + "\n\n" + jsonData.data["serverLogMonstre"] + "\n\n";
        messageCombat += "Vie joueur: " + jsonData.data["vieJoueur"] + " | Vie monstre: " + jsonData.data["vieMonstre"];
        if(jsonData.data["vieMonstre"] != 0 && jsonData.data["vieJoueur"] !=0) {
            messageCombat += "\n\n\tActions disponibles: \n" + attackUser + "\tObjets disponibles:\n" + objetUser;
            messageCombat += "\nQue voulez-vous faire?\n";
        }

        combatBox.value = messageCombat;
    }

    if(jsonData.serverPacket==="endCombat") {
        console.log("On reçois endCombat");
        var combatBox = document.getElementById("messagesTextArea");
        messageCombat = jsonData.data["serverLogJoueur"] + "\n" + jsonData.data["serverLogMonstre"];
        if (jsonData.data["objetDrop"]!=null) {
            messageCombat += "\n\nVous avez gagné " + jsonData.data["objetDrop"] + " a votre inventaire!\n";
        }
        combatBox.value = messageCombat
        chat=true;
        keydown=true;
        dessinMap = setInterval(function() {
            map.dessinerMap(ctx);
        }, 40);
    }
};

websocket.onerror = function () {
    console.log('Connection error');
};

function sendMessage() {
    var textMsg = document.getElementById("messageText");
    if(chat) {
        websocket.send(JSON.stringify({"clientPacket": "chatMsg", "data": textMsg.value})); //A tester
        console.log("On envoi au serveur: " + textMsg.value);
        textMsg.value = "";
    }
    else {
        console.log("On envoi " + textMsg.value);
        websocket.send(JSON.stringify({
            "clientPacket":"combatMsg",
            "data":{
                surnom: {
                    "action": textMsg.value
                }
            }
        }));
        textMsg.value = "";
    }
}

window.onbeforeunload = function () {
    websocket.onclose = function () {};
    websocket.close();
};