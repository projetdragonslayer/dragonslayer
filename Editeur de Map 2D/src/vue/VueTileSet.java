package vue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.ModelMoteur;

public class VueTileSet extends JPanel{	
	protected JLabel taskBar;
	protected JLabel test;
	
	public BufferedImage dbImage; // On consid�re 1 case de tileset fait 16x16px
	//protected JPanel panLeft; //Aper�u du tileset charg� par d�faut
	protected JPanel panCenter; //Aper�u de notre map
	protected JPanel menuBar;
	
	ModelMoteur myModel;
	
	public VueTileSet (ModelMoteur myModel) {
		this.myModel = myModel;
		//panLeft = new JPanel(new GridLayout(10,10));
		//panCenter = new JPanel(new GridLayout(10,10));
		menuBar = new JPanel(new FlowLayout());
		setLayout(new BorderLayout());
		
		try {
			dbImage = ImageIO.read(getClass().getResource("/tileset/mytileset.png"));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
						
		//test.setIcon(ii);
		
		this.addMouseListener(myModel.myListener);
		
		add(menuBar, BorderLayout.SOUTH);
		//add(panCenter, BorderLayout.CENTER);
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int i, j, k;
		int nbPxX= myModel.nbPxX;
		int nbPxY= myModel.nbPxY;
		int nbCaseColumn = getNbCaseTileSetWidth(nbPxX, dbImage.getWidth());
		int nbCaseLine = getNbCaseTileSetHeight(nbPxY, dbImage.getHeight());

		g.drawImage(dbImage, 0, 0, dbImage.getWidth(), dbImage.getHeight(), this);
		
		for (i = 0; i < nbCaseLine; i++) {
			for (j = 0; j < nbCaseColumn; j++) {
				g.drawRect(j * nbPxX, i * nbPxY, nbPxX, nbPxY);
			}
		}
	}
		
	public int getNbCaseTileSetWidth(int tileWidth, int tileSetWidth) {
		return tileSetWidth/tileWidth;
	}
	
	public int getNbCaseTileSetHeight(int tileHeight, int tileSetHeight) {
		return tileSetHeight/tileHeight;
	}
}