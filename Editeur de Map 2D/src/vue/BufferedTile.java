package vue;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

public class BufferedTile {
	private BufferedImage tileImage;
	int tilePosC; //correspond au numero de colonne de la tile dans le tileSet
	int tilePosL; //correspond au numero de ligne de la tile dans le tileSet
	int indiceC; //ne sera plus nécessaire avec les niveaux de tile
	int indiceL; //ne sera plus nécessaire avec les niveaux de tile
	public boolean vide;
	
	public BufferedTile() {
		vide = true;
	}
	
	public BufferedTile(int indiceL, int indiceC) {
		this.indiceC = indiceC;
		this.indiceL = indiceL;
		vide = true;
	}
	
	public void setTileCoord(int column, int line) {
		tilePosC = column;
		tilePosL = line;
		vide = false;
	}
	
	public void setImage(BufferedImage myImage) {
		tileImage = myImage;
	}
	
	public void setTile(BufferedTile aTile) { //Copie le contenu d'une Tile dans la cible
		tilePosC = aTile.tilePosC;
		tilePosL = aTile.tilePosL;
		tileImage = aTile.getImage();
		vide = false;
	}
	
	public void setMapCoord(int numColumn, int numLine) {
		indiceC = numColumn;
		indiceL = numLine;
		vide = false;
	}
	
	public int getTileColumn() {
		return tilePosC;
	}
	
	public int getTileLine() {
		return tilePosL;
	}
	
	public int getIndiceColumn() { //A ne pas utiliser avec les CompleteTile
		return indiceC;
	}
	
	public int getIndiceLine() { //A ne pas utiliser avec les CompleteTile
		return indiceL;
	}
	
	public BufferedImage getImage() {
		return tileImage;
	}
	/*
	public boolean isEmpty() {

	}*/
}
