package vue;

public class CompleteTile {
	private BufferedTile tile1;
	private BufferedTile tile2;
	private BufferedTile tile3;
	int indiceC;
	int indiceL;
	public boolean vide;
	
	public CompleteTile () {
		vide = true;
	}
	
	public CompleteTile(int line, int column) {
		indiceC = column;
		indiceL = line;
		vide = true;
	}
	
	public void setTile(BufferedTile aTile, int tileLevel) {
		switch (tileLevel) {
			case 1:
				tile1 = new BufferedTile();
				tile1.setTile(aTile);
				break;
			case 2:
				tile2 = new BufferedTile();
				tile2.setTile(aTile);
				break;
			case 3:
				tile3 = new BufferedTile();
				tile3.setTile(aTile);
				break;
		}
		vide = false;
	}
	
	public BufferedTile getTile(int indiceTile) {
		switch (indiceTile) {
			case 1:
				return tile1;
			case 2:
				return tile2;
			case 3:
				return tile3;
			default:
				return null;
		}
	}
	
	public int getIndiceColumn() { 
		return indiceC;
	}
	
	public int getIndiceLine() { 
		return indiceL;
	}
}
