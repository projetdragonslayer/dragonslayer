package vue;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import ecouteur.Ecouteur;
import model.ModelMoteur;

public class VueImporteur extends JPanel{
	protected JLabel title;
	protected JLabel labelNomFichier;
	protected JLabel labelTypeFichier;
	
	protected JPanel panNorth;
	protected JPanel panCenter;
	
	public JTextField nomFichier;

	protected JButton valider;

	protected int taillePolice;
	ModelMoteur myModel;

	public VueImporteur (ModelMoteur myModel) {
		this.myModel = myModel;
		taillePolice = 25;
		title = new JLabel("Importeur de map : ");
		labelNomFichier = new JLabel("Nom du fichier : ");
		labelTypeFichier = new JLabel("Type du fichier : ");
		panNorth =new JPanel(new FlowLayout(FlowLayout.CENTER));
		panCenter = new JPanel(new GridLayout(4, 2));
		nomFichier = new JTextField();
		valider = new JButton("Valider");
		valider.setName("valider_importeur");
		
		setLayout(new BorderLayout());

		title.setPreferredSize(new Dimension(500, 100));
		title.setFont(new Font("Roman", Font.BOLD,taillePolice));
		title.setOpaque(false);
		
		labelNomFichier.setFont(new Font("Roman", Font.BOLD,taillePolice));
		labelNomFichier.setOpaque(false);
		
		labelTypeFichier.setFont(new Font("Roman", Font.BOLD,taillePolice));
		labelTypeFichier.setOpaque(false);
				
		nomFichier.setPreferredSize(new Dimension(500, 100));
		nomFichier.setFont(new Font("Roman", Font.BOLD,taillePolice));
		nomFichier.setOpaque(false);
				
		valider.setPreferredSize(new Dimension(500, 100));
		valider.setFont(new Font("Roman", Font.BOLD,taillePolice));
		valider.setOpaque(false);

		valider.addActionListener(myModel.myListener);
		
		panNorth.add(title);
		
		panCenter.add(labelNomFichier);
		panCenter.add(nomFichier);
		panCenter.add(labelTypeFichier);
		
		add(panNorth, BorderLayout.NORTH);
		add(panCenter, BorderLayout.CENTER);
		add(valider, BorderLayout.SOUTH);
	}
}
