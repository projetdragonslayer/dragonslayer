package vue;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import ecouteur.Ecouteur;
import model.ModelMoteur;

public class VueParametre extends JPanel{
	protected JLabel title;
	protected JLabel labelForColumn;
	protected JLabel labelForLine;
	protected JLabel labelForNbPxX;
	protected JLabel labelForNbPxY;

	protected JPanel panNorth;
	protected JPanel panCenter;
	protected JPanel menuBar;

	public JTextField nbColumn;
	public JTextField nbLine;
	protected JTextField nbPxX;
	protected JTextField nbPxY;
	protected JButton valider;

	protected int taillePolice;
	ModelMoteur myModel;

	public VueParametre (ModelMoteur myModel) {
		this.myModel = myModel;
		taillePolice = 25;
		title = new JLabel("Taille de la map : ");
		labelForColumn = new JLabel("Nombre de colonne : ");
		labelForLine = new JLabel("Nombre de Ligne : ");
		labelForNbPxX = new JLabel("Nombre de Px d'une case du tileset (largeur) : ");
		labelForNbPxY = new JLabel("Nombre de Px d'une case du tileset (hauteur) : ");
		panNorth = new JPanel(new FlowLayout(FlowLayout.CENTER));
		panCenter = new JPanel(new GridLayout(4, 2));
		nbColumn = new JTextField("10");
		nbLine = new JTextField("10");
		nbPxX = new JTextField("NE FONCTIONNE PAS");
		nbPxY = new JTextField("NE FONCTIONNE PAS");
		valider = new JButton("Valider");
		valider.setName("valider_parametre");
		
		setLayout(new BorderLayout());

		title.setPreferredSize(new Dimension(500, 100));
		title.setFont(new Font("Roman", Font.BOLD,taillePolice));
		title.setOpaque(false);
		
		labelForColumn.setFont(new Font("Roman", Font.BOLD,taillePolice));
		labelForColumn.setOpaque(false);
		
		labelForLine.setFont(new Font("Roman", Font.BOLD,taillePolice));
		labelForLine.setOpaque(false);
		
		labelForNbPxX.setFont(new Font("Roman", Font.BOLD,taillePolice));
		labelForNbPxX.setOpaque(false);
		
		labelForNbPxY.setFont(new Font("Roman", Font.BOLD,taillePolice));
		labelForNbPxY.setOpaque(false);
		
		nbColumn.setPreferredSize(new Dimension(500, 100));
		nbColumn.setFont(new Font("Roman", Font.BOLD,taillePolice));
		nbColumn.setOpaque(false);
		
		nbLine.setPreferredSize(new Dimension(500, 100));
		nbLine.setFont(new Font("Roman", Font.BOLD,taillePolice));
		nbLine.setOpaque(false);
		
		nbPxX.setPreferredSize(new Dimension(500, 100));
		nbPxX.setFont(new Font("Roman", Font.BOLD,taillePolice));
		nbPxX.setOpaque(false);

		nbPxY.setPreferredSize(new Dimension(500, 100));
		nbPxY.setFont(new Font("Roman", Font.BOLD,taillePolice));
		nbPxY.setOpaque(false);
		
		valider.setPreferredSize(new Dimension(500, 100));
		valider.setFont(new Font("Roman", Font.BOLD,taillePolice));
		valider.setOpaque(false);

		valider.addActionListener(myModel.myListener);
		
		panNorth.add(title);
		
		panCenter.add(labelForColumn);
		panCenter.add(nbColumn);
		panCenter.add(labelForLine);
		panCenter.add(nbLine);
		panCenter.add(labelForNbPxX);
		panCenter.add(nbPxX);
		panCenter.add(labelForNbPxY);
		panCenter.add(nbPxY);
		
		add(panNorth, BorderLayout.NORTH);
		add(panCenter, BorderLayout.CENTER);
		add(valider, BorderLayout.SOUTH);
	}
}
