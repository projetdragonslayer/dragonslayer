package vue;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import ecouteur.Ecouteur;
import model.ModelMoteur;

public class VueInterfaceBasic extends JPanel{
	//protected BufferedImage dbImage; //Ne sert plus a rien
	protected BufferedTile tempTile;
	protected CompleteTile tempBigTile;
	//protected JPanel panLeft; //Aper�u du tileset charg� par d�faut
	protected JPanel panCenter; //Aper�u de notre map
	public JPanel menuBar;
	
	protected JButton importMap;
	protected JButton exportMap;
	public JButton openTileset;
	
	public JLabel tileLevelLabel;
	protected JButton tileLevelUp;
	protected JButton tileLevelDown;
		
	ModelMoteur myModel;
	
	public VueInterfaceBasic (ModelMoteur myModel) {
		this.myModel = myModel;
		panCenter = new JPanel(new GridLayout(10,10));
		menuBar = new JPanel(new FlowLayout());
		openTileset = new JButton("Open Tileset");
		importMap = new JButton("Import Map");
		exportMap = new JButton("Export Map");
		
		tileLevelLabel = new JLabel("Niveau de tile : 1");
		tileLevelUp = new JButton ("TileLvL ++");
		tileLevelDown = new JButton ("TileLvL --");
		setLayout(new BorderLayout());
		
		openTileset.setPreferredSize(new Dimension(300, 100));
		importMap.setPreferredSize(new Dimension(300, 100));
		exportMap.setPreferredSize(new Dimension(300, 100));
		tileLevelLabel.setPreferredSize(new Dimension(100, 100));
		tileLevelUp.setPreferredSize(new Dimension(100, 100));
		tileLevelDown.setPreferredSize(new Dimension(100, 100));
		
		openTileset.addActionListener(myModel.myListener);
		importMap.addActionListener(myModel.myListener);
		exportMap.addActionListener(myModel.myListener);
		tileLevelUp.addActionListener(myModel.myListener);
		tileLevelDown.addActionListener(myModel.myListener);
		addMouseListener(myModel.myListener);
		
		openTileset.setOpaque(false);
		importMap.setOpaque(false);
		exportMap.setOpaque(false);
		panCenter.setOpaque(false);
		menuBar.setOpaque(false);
		
		menuBar.add(importMap);
		menuBar.add(exportMap);
		menuBar.add(tileLevelLabel);
		menuBar.add(tileLevelUp);
		menuBar.add(tileLevelDown);
		menuBar.add(openTileset);

		add(menuBar, BorderLayout.SOUTH);
		add(panCenter, BorderLayout.CENTER);
	}
	/*
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int i, j;
		
		for (i = 0; i < myModel.nbLine; i++) {
			for (j = 0; j < myModel.nbColumn; j++) {
				tempTile = myModel.tabMap[i][j];
				if (!(tempTile.vide))
					g.drawImage(tempTile.getImage(), tempTile.getIndiceColumn() * myModel.getNbPxLine(), tempTile.getIndiceLine() * myModel.getNbPxLine(), myModel.getNbPxLine(), myModel.getNbPxLine(), this);
				}
		}
	}
	*/
	// Version avec la gestion des niveaux de Tile
	 
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int i, j, k;
		int nbPxLine = myModel.getNbPxLine();
		
		for (i = 0; i < myModel.nbLine; i++) {
			for (j = 0; j < myModel.nbColumn; j++) {
				tempBigTile = myModel.tabMap[i][j];
				for (k = 0; k < 3; k++) {
					tempTile = tempBigTile.getTile(k + 1);
					if (!(tempTile == null))
						g.drawImage(tempTile.getImage(), tempBigTile.getIndiceColumn() * nbPxLine, tempBigTile.getIndiceLine() * nbPxLine, nbPxLine, nbPxLine, this); //(image, x, y, nbPxdeLargeur, nbPxdeHauteur)
				}
			}
		}
		
		for (i = 0; i < myModel.nbLine; i++) {
			for (j = 0; j < myModel.nbColumn; j++) {
				g.drawRect(i * nbPxLine, j * nbPxLine, nbPxLine, nbPxLine);
			}
		}
	}
	
	public void loadTile() {
		paintComponent(this.getGraphics());
	}
	
	/*
	public void setTileToLoad(int line, int column) { // A ne pas utiliser
		int myX = 0;
		int myY = 0;
		int posX = 0;
		int posY = 0;
		
		try {
			dbImage = ImageIO.read(getClass().getResource("/tileset/mytileset.png"));
			
			myX = line *  myModel.nbPxX;
			myY = column *  myModel.nbPxY;
			posX = myX -  myModel.nbPxX;
			posY = myY -  myModel.nbPxY;
				
			dbImage = dbImage.getSubimage(posX, posY,  myModel.nbPxX,  myModel.nbPxY);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		 myY = 0;
	}*/
	
	/*
	public void setActualTileByCase(int mapCaseC, int mapCaseL, int tileSetNum) { // Inutile
		int myX = 0;
		int myY = 0;
		int posX = 0;
		int posY = 0;
		
		try {
			dbImage = ImageIO.read(getClass().getResource("/tileset/mytileset.png"));
			
			myX = mapCaseC *  myModel.nbPxX;
			myY = mapCaseL *  myModel.nbPxY;
			posX = myX -  myModel.nbPxX;
			posY = myY -  myModel.nbPxY;
				
			dbImage = dbImage.getSubimage(posX, posY,  myModel.nbPxX,  myModel.nbPxY);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setActualTileByXY(int x, int y) { //SET la tile � placer dans votre map
		int myX = 0;
		int myY = 0;
		int posX = 0;
		int posY = 0;
		
		try {
			dbImage = ImageIO.read(getClass().getResource("/tileset/mytileset.png"));
			
			myX = (int)(Math.ceil(x/((double) myModel.nbPxX))) *  myModel.nbPxX;
			myY = (int) (Math.ceil(y/((double) myModel.nbPxY))) *  myModel.nbPxX;
			posX = (int) myX -  myModel.nbPxX;
			posY = (int) myY -  myModel.nbPxY;
				
			dbImage = dbImage.getSubimage(posX, posY, myModel.nbPxX, myModel.nbPxY);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}*/
	
	public int getNbCaseTileSetWidth(int tileWidth, int tileSetWidth) {
		return tileSetWidth/tileWidth;
	}
	
	public int getNbCaseTileSetHeight(int tileHeight, int tileSetHeight) {
		return tileSetHeight/tileHeight;
	}
}