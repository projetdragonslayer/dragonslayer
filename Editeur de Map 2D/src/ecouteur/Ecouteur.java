package ecouteur;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;
import model.*;

public class Ecouteur implements ActionListener, KeyListener, MouseListener {
	private ModelMoteur myModel;
	
	public Ecouteur (ModelMoteur myModel) {	
		this.myModel = myModel;
	}
	
	public void actionPerformed (ActionEvent e) {
		JTextField myTxtF;
		JButton myJB;
		
		if (e.getSource() instanceof JButton) {
			myJB = (JButton) e.getSource();
			
			if (myJB.getText() == "Open Tileset")
				myModel.changeView(ModelMoteur.VUE_TILESET);
			else if (myJB.getText() == "Import Map") {
				myModel.changeView(ModelMoteur.VUE_IMPORTEUR);
			}
			else if (myJB.getText() == "Export Map") {
				myModel.changeView(ModelMoteur.VUE_EXPORTEUR);
			}
			else if (myJB.getText() == "TileLvL ++") {
				myModel.tileLevelUp();
			}
			else if (myJB.getText() == "TileLvL --") {
				myModel.tileLevelDown();
			}
			else if (myJB.getName() == "valider_parametre") {
				myModel.changeView(ModelMoteur.VUE_BASIC);
				myModel.setTabMap(Integer.parseInt(myModel.myVueParametre.nbColumn.getText()), Integer.parseInt(myModel.myVueParametre.nbLine.getText()));
			}
			else if (myJB.getName() == "valider_exporteur") {
				myModel.changeView(ModelMoteur.VUE_BASIC);
				myModel.exportMap(myModel.myVueExporteur.nomFichier.getText());
			}
			else if (myJB.getName() == "valider_importeur") {
				myModel.changeView(ModelMoteur.VUE_BASIC);
				myModel.importMap(myModel.myVueImporteur.nomFichier.getText());
			}
		}
	}
	
	public void mouseClicked(MouseEvent e) {

	}
	
	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		if (myModel.fen.getHeight() == ModelMoteur.height) { //Determine dans quelle vue on est
			myModel.setBufferedTile(arg0.getX(), arg0.getY());
		}
		else {
			myModel.setTempTileByXY(arg0.getX(), arg0.getY());
			myModel.changeView(ModelMoteur.VUE_BASIC);
		}		
	};
	
	public void keyPressed(KeyEvent e){};

	public void	keyReleased(KeyEvent e){};

	public void	keyTyped(KeyEvent e){}
}
