package application;

import java.io.IOException;

import javax.swing.JFrame;

import ecouteur.*;
import model.*;
import vue.*;

public class Appli {
	public static void main (String args []) {
		
		JFrame fen = new JFrame("Editeur de map 2D");
		fen.setSize(ModelMoteur.width, ModelMoteur.height);
		fen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		ModelMoteur myModel = new ModelMoteur();
		myModel.setFen(fen);
		
		Ecouteur myListener = new Ecouteur(myModel);
		myModel.setListener(myListener);
		
		VueParametre myVueParametre = new VueParametre(myModel);	
		myModel.setVueParametre(myVueParametre);
		
		VueInterfaceBasic vue = new VueInterfaceBasic(myModel);
		myModel.setVue(vue);
		
		VueTileSet vueTileSet = new VueTileSet(myModel);
		myModel.setVueTileSet(vueTileSet);
		
		VueExporteur vueExporteur = new VueExporteur(myModel);
		myModel.setVueExporteur(vueExporteur);
		
		VueImporteur vueImporteur = new VueImporteur(myModel);
		myModel.setVueImporteur(vueImporteur);
		
		//vue.setActualTileByNum(2);
		//vue.setActualTileByXY(30,15);
		
		fen.setContentPane(myVueParametre);
		fen.setResizable(false);
		
		fen.setVisible(true);
	}
}
