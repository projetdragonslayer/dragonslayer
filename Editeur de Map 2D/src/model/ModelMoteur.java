package model;

import ecouteur.Ecouteur;
import vue.*;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

public class ModelMoteur implements InterfaceModelMoteur{
	//public BufferedTile tabMap[][];
	public CompleteTile tabMap[][]; //A ajouter lorsque les Tiles � multiple niveau sont completement implenter
	public int nbLine;
	public int nbColumn;
	public int nbPxX;
	public int nbPxY;
	public JFrame fen;
	public VueInterfaceBasic myVue;
	public VueTileSet myVueTileSet;
	public VueParametre myVueParametre;
	public VueExporteur myVueExporteur;
	public VueImporteur myVueImporteur;
	public Ecouteur myListener;
	public BufferedTile tempTile;
	public int tileLevel;
	
	public ModelMoteur () {
		tempTile = new BufferedTile();
		nbPxX = 16;
		nbPxY = 16;
		tileLevel = 1;
	}
	
	public void setVue(VueInterfaceBasic myVue) {
		this.myVue = myVue;
	}
	
	public void setListener(Ecouteur myListener) {
		this.myListener = myListener;
	}
	
	public void setFen(JFrame fen) {
		this.fen = fen;
	}
	
	public void setVueTileSet(VueTileSet myVueTileSet) {
		this.myVueTileSet = myVueTileSet;
	}
	
	
	public void setVueParametre(VueParametre myVueParametre) {
		this.myVueParametre = myVueParametre;
	}

	public void	setVueExporteur(VueExporteur myVueExporteur) {
		this.myVueExporteur = myVueExporteur;
	}
	
	public void	setVueImporteur(VueImporteur myVueImporteur) {
		this.myVueImporteur = myVueImporteur;
	}
	
	
	public void tileLevelUp() {
		if (tileLevel < 3) {
			tileLevel++;
			myVue.tileLevelLabel.setText("Niveau de tile : " + tileLevel);
		}
	}
	
	public void tileLevelDown() {
		if (tileLevel > 1) {
			tileLevel--;
			myVue.tileLevelLabel.setText("Niveau de tile : " + tileLevel);
		}
	}
	/*
	public void importMap() {
		String line;
		int i = 0;
		int j = 0;
		int tileL = 0;
		int tileC = 0;
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader("default.txt"));

			line = reader.readLine();
			nbLine = Integer.parseInt(line.substring(line.indexOf('=') + 2));
			
			line = reader.readLine();
			nbColumn = Integer.parseInt(line.substring(line.indexOf('=') + 2));
			
			setTabMap(nbColumn, nbLine); //pour r�initialiser toutes la map
			
			while ((line = reader.readLine()) != null) {
				if (!(line.equals( "{") || line.equals("}") || line.equals( "FIN_MAP"))) {
					if ("i".equals(line.substring(0, line.indexOf('=') - 1)))
						i = Integer.parseInt(line.substring(line.indexOf('=') + 2));
					else if ("j".equals(line.substring(0, line.indexOf('=') - 1))) {
						j = Integer.parseInt(line.substring(line.indexOf('=') + 2));
						tabMap[i][j].setMapCoord(j, i);
					}
					else if ("tilePosC".equals(line.substring(0, line.indexOf('=') - 1)))
						tileC = Integer.parseInt(line.substring(line.indexOf('=') + 2));
					else if ("tilePosL".equals(line.substring(0, line.indexOf('=') - 1))) {
						tileL = Integer.parseInt(line.substring(line.indexOf('=') + 2));
						tabMap[i][j].setTileCoord(tileC, tileL);
					}
				}
			}
			reader.close();
		}
		catch (FileNotFoundException fe) {
			fe.printStackTrace();
		}
		catch (UnsupportedEncodingException ue) {
			ue.printStackTrace();
		}
		catch (IOException e) {
			System.err.format("Exception occurred trying to read '%s'.", "default.txt");
			e.printStackTrace();
		}
		reloadAllTile();		
	}*/
	
	// Version avec la gestion des niveaux de Tile
	 
	public void importMap(String nomFichier) {
		String line;
		BufferedTile tempTile = null;
		int i = 0;
		int j = 0;
		int k = 0;
		int tileL = 0;
		int tileC = 0;
		int tempIndice = 0;
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(nomFichier + ".map"));

			line = reader.readLine();
			nbLine = Integer.parseInt(line.substring(line.indexOf('=') + 2));
			
			line = reader.readLine();
			nbColumn = Integer.parseInt(line.substring(line.indexOf('=') + 2));
			
			setTabMap(nbColumn, nbLine); //pour r�initialiser toutes la map
			
			while ((line = reader.readLine()) != null) {
				//if (!(line.equals( "{") || line.equals("}") || line.equals( "FIN_MAP"))) {
				if (line.equals("{")) {
					line = reader.readLine();
					i = Integer.parseInt(line.substring(line.indexOf('=') + 2));
					line = reader.readLine();
					j = Integer.parseInt(line.substring(line.indexOf('=') + 2));
					tabMap[i][j] = new CompleteTile(i, j);
					
					while ((!(line = reader.readLine()).equals("}"))) {
						if ("tileIndice".equals(line.substring(0, line.indexOf('=') - 1))) {
							tempIndice = Integer.parseInt(line.substring(line.indexOf('=') + 2));
						}
						else if ("tilePosC".equals(line.substring(0, line.indexOf('=') - 1)))
							tileC = Integer.parseInt(line.substring(line.indexOf('=') + 2));
						else if ("tilePosL".equals(line.substring(0, line.indexOf('=') - 1))) {
							tileL = Integer.parseInt(line.substring(line.indexOf('=') + 2));
							tempTile = new BufferedTile();
							tempTile.setTileCoord(tileC, tileL);
							tabMap[i][j].setTile(tempTile, tempIndice);
						}
					}
				}
			}
			reader.close();
		}
		catch (FileNotFoundException fe) {
			fe.printStackTrace();
		}
		catch (UnsupportedEncodingException ue) {
			ue.printStackTrace();
		}
		catch (IOException e) {
			System.err.format("Exception occurred trying to read '%s'.", "default.txt");
			e.printStackTrace();
		}
		reloadAllTile();		
	}
	
	/*
	public void exportMap() {
		BufferedTile tempTile;
		try {
			PrintWriter writer = new PrintWriter("default.txt", "UTF-8");
			int i, j;
			
			writer.println("nbLine = " + nbLine);
			writer.println("nbColumn = " + nbColumn);
			
			for (i = 0; i < nbLine; i++) {
				for (j = 0; j < nbColumn; j++) {
					tempTile = tabMap[i][j];
					if (tempTile.vide == false) {
						writer.println("{");
						writer.println("i = " + i);
						writer.println("j = " + j);
						writer.println("tilePosC = " + tempTile.getTileColumn());
						writer.println("tilePosL = " + tempTile.getTileLine());
						writer.println("}");
					}
				}
			}
			writer.println("FIN_MAP");
			writer.close();
		}
		catch (FileNotFoundException fe) {
			fe.printStackTrace();
		}
		catch (UnsupportedEncodingException ue) {
			ue.printStackTrace();
		}
	}
	*/
	// Version avec la gestion des niveaux de Tile
	 
	public void exportMap(String nomFichier) {
		CompleteTile tempBigTile;
		BufferedTile tempTile;
		
		try {
			PrintWriter writer = new PrintWriter(nomFichier + ".map", "UTF-8");
			int i, j, k, tempInd;
			
			writer.println("nbLine = " + nbLine);
			writer.println("nbColumn = " + nbColumn);
			
			for (i = 0; i < nbLine; i++) {
				for (j = 0; j < nbColumn; j++) {
					tempBigTile = tabMap[i][j];
					if (!tempBigTile.vide) {
						writer.println("{");
						writer.println("i = " + i);
						writer.println("j = " + j);
						for (k = 0; k < 3; k++) {
							tempTile = tempBigTile.getTile(k + 1);
							if (!(tempTile == null)) {
								tempInd = k + 1;
								writer.println("tileIndice = " + tempInd);
								writer.println("tilePosC = " + tempTile.getTileColumn());
								writer.println("tilePosL = " + tempTile.getTileLine());
							}
						}
						writer.println("}");
					}
				}
			}
			writer.println("FIN_MAP");
			writer.close();
		}
		catch (FileNotFoundException fe) {
			fe.printStackTrace();
		}
		catch (UnsupportedEncodingException ue) {
			ue.printStackTrace();
		}
	}
	
	/* 
	//EXPORTEUR JAVASCRIPT
	public void exportMap() {
		CompleteTile tempBigTile;
		BufferedTile tempTile;
		
		try {
			PrintWriter writer = new PrintWriter("default.txt", "UTF-8");
			int i, j, k, tempInd;
			
			writer.println("nbLine = " + nbLine);
			writer.println("nbColumn = " + nbColumn);
			
			for (i = 0; i < nbLine; i++) {
				for (j = 0; j < nbColumn; j++) {
					tempBigTile = tabMap[i][j];
					if (!tempBigTile.vide) {
						writer.println("{");
						writer.println("i = " + i);
						writer.println("j = " + j);
						for (k = 0; k < 3; k++) {
							tempTile = tempBigTile.getTile(k + 1);
							if (!(tempTile == null)) {
								tempInd = k + 1;
								writer.println("tileIndice = " + tempInd);
								writer.println("ts.dessinerTile(1" + tempTile.getTileColumn() + "1, ctx, 10, 10);");
								writer.println("tilePosL = " + tempTile.getTileLine());
							}
						}
						writer.println("}");
					}
				}
			}
			writer.println("FIN_MAP");
			writer.close();
		}
		catch (FileNotFoundException fe) {
			fe.printStackTrace();
		}
		catch (UnsupportedEncodingException ue) {
			ue.printStackTrace();
		}
	}
    ts.dessinerTile(1, ctx, 10, 10);
    ts.dessinerTile(5, ctx, 50, 10);
    ts.dessinerTile(6, ctx, 90, 10);
    ts.dessinerTile(7, ctx, 130, 10);
	*/
	
	/*
	public void reloadAllTile() {
		int i;
		int j;
		int column, line;
		int myX = 0;
		int myY = 0;
		BufferedImage tempImage;
				
		for (i = 0; i < nbLine; i++) {
			for (j = 0; j < nbColumn; j++) {
				if (!(tabMap[i][j].vide)) {
					try {
						tempImage = ImageIO.read(getClass().getResource("/tileset/mytileset.png"));					
						
						myX = (int) tabMap[i][j].getTileColumn() * nbPxX;
						myY = (int) tabMap[i][j].getTileLine() * nbPxY;
						
						tempImage = tempImage.getSubimage(myX, myY, nbPxX, nbPxY);
						tabMap[i][j].setImage(tempImage);
					}
					catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		refreshScreen();
	}
	*/
	// Version avec la gestion des niveaux de Tile
	 
	public void reloadAllTile() {
		int i,j,k;
		int column, line;
		int myX = 0;
		int myY = 0;
		BufferedImage tempImage;
		CompleteTile tempBigTile;
				
		for (i = 0; i < nbLine; i++) {
			for (j = 0; j < nbColumn; j++) {
				tempBigTile = tabMap[i][j];
				for (k = 0; k < 3; k++) {
					tempTile = tempBigTile.getTile(k + 1);
					if (!(tempTile == null)) { //A modifier je pense
						try {
							tempImage = ImageIO.read(getClass().getResource("/tileset/mytileset.png"));					
							
							myX = (int) tempTile.getTileColumn() * nbPxX;
							myY = (int) tempTile.getTileLine() * nbPxY;
							
							tempImage = tempImage.getSubimage(myX, myY, nbPxX, nbPxY);
							tempTile.setImage(tempImage);
						}
						catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		refreshScreen();
	}

		
	public void setTempTileByXY(int x, int y) { //SET la tile � placer dans votre map (A n'appeler que dans la VueTileSet)
		int column, line;
		
		int myX = 0;
		int myY = 0;
		BufferedImage tempImage;
		tempTile = new BufferedTile();
		
		try {
			tempImage = ImageIO.read(getClass().getResource("/tileset/mytileset.png"));
			
			column = (int) (x/((double) nbPxX));
			line = (int) (y/((double) nbPxY));
			
			tempTile.setTileCoord(column, line);
			
			myX = (int) column * nbPxX;
			myY = (int) line * nbPxY;
			
			tempImage = tempImage.getSubimage(myX, myY, nbPxX, nbPxY);
			tempTile.setImage(tempImage);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		
		
		fen.repaint();
		fen.setVisible(true);
	}
	/*	
	public void setBufferedTile(int x, int y) { //Permet de changer l'image d'une case de la map sur celle actuellement s�lectionner dans le tileSet
		BufferedTile myTile;
		
		myTile = getTileByXY(x, y);
		myTile.setTile(tempTile);
		refreshScreen();
	}
	*/
	// Version avec la gestion des niveaux de Tile
	 
	public void setBufferedTile(int x, int y) { //Permet de changer l'image d'une case de la map sur celle actuellement s�lectionner dans le tileSet
		CompleteTile myTile;
		
		myTile = getTileByXY(x, y);
		myTile.setTile(tempTile, tileLevel);
		refreshScreen();
	}

	/*
	public void setTabMap(int nbCaseC, int nbCaseL) {
		int i;
		int j;
		
		this.nbLine = nbCaseL;
		this.nbColumn = nbCaseC;
		
		tabMap = new BufferedTile [nbLine][nbColumn];
		for (i = 0; i < nbLine; i++) {
			for (j = 0; j < nbColumn; j++) {
				tabMap[i][j] = new BufferedTile(i, j);
			}
		}
	}
	*/
	// Version avec la gestion des niveaux de Tile
	 
	public void setTabMap(int nbCaseC, int nbCaseL) {
		int i;
		int j;
		
		this.nbLine = nbCaseL;
		this.nbColumn = nbCaseC;
		
		tabMap = new CompleteTile[nbLine][nbColumn];
		for (i = 0; i < nbLine; i++) {
			for (j = 0; j < nbColumn; j++) {
				tabMap[i][j] = new CompleteTile(i, j);
			}
		}
	}
	
	/*
	public BufferedTile getTileByXY(int x, int y) {
		int column;
		int line;
				
		column = (int) (x/((double) getNbPxLine()));
		line = (int) (y/((double) getNbPxLine()));
		return tabMap[line][column];
	}*/
	
	// Version avec la gestion des niveaux de Tile
	 
	public CompleteTile getTileByXY(int x, int y) {
		int column;
		int line;
				
		column = (int) (x/((double) getNbPxLine()));
		line = (int) (y/((double) getNbPxLine()));
		//column = (int) column - nbPxX;
		//line = (int) line - nbPxY;
		return tabMap[line][column];
	}

	/*
	public BufferedTile getTileByColLine(int Column, int Line) {
		return tabMap[Line][Column];
	}*/
	
	// Version avec la gestion des niveaux de Tile
	 
	public CompleteTile getTileByColLine(int Column, int Line) {
		return tabMap[Line][Column];
	}
		
	public void refreshScreen() {
		myVue.loadTile();
	}
	
	public int getNbPxColumn() { //Renvoie la largeur d'une case de la map
		return ModelMoteur.width/nbColumn;
	}
	
	public int getNbPxLine() { //Renvoie la hauteur d'une case de la map
		return (ModelMoteur.height - (myVue.menuBar.getHeight() + 50))/nbLine;
	}
	
	public void changeView (int nomVue) {
		switch (nomVue) {
			case VUE_BASIC:
				fen.setContentPane(myVue);
				fen.setSize(ModelMoteur.width, ModelMoteur.height);
				break;
			case VUE_TILESET:
				fen.setContentPane(myVueTileSet);
				fen.setSize(myVueTileSet.dbImage.getWidth(), myVueTileSet.dbImage.getHeight());
				break;
			case VUE_PARAMETRE:
				fen.setContentPane(myVueParametre);
				fen.setSize(ModelMoteur.width, ModelMoteur.height);
				break;
			case VUE_EXPORTEUR:
				fen.setContentPane(myVueExporteur);
				fen.setSize(ModelMoteur.width, ModelMoteur.height);
				break;
			case VUE_IMPORTEUR:
				fen.setContentPane(myVueImporteur);
				fen.setSize(ModelMoteur.width, ModelMoteur.height);
				break;
		}
		
		fen.repaint();
		fen.setVisible(true);
	}
}
