package model;

public interface InterfaceModelMoteur {
	public final static int VUE_BASIC = 0;
	public final static int VUE_TILESET = 1;
	public final static int VUE_PARAMETRE = 2;
	public final static int VUE_EXPORTEUR = 3;
	public final static int VUE_IMPORTEUR = 4;
	public int width = 1280;
	public int height = 720;
}
