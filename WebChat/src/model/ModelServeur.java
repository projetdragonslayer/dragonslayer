package model;

import map.Case_map;
import map.GameMap;
import personnage.Personnage;

/**
 * Created by Sandy on 23/04/2016.
 */
public class ModelServeur {
	public final static int BAS = 0;
	public final static int GAUCHE = 1;
	public final static int DROITE = 2;
	public final static int HAUT = 3;
	public final static int MAX_JOUEUR = 24;
	
	//public final static Case_map assetMap1[][] = new Case_map[][] {
	
	protected Personnage joueur[];
	protected GameMap map1;
	
    public ModelServeur() {
    	map1 = new GameMap(38, 18);
    	map1.loadMapAsset("dungeon");
    	joueur = new Personnage[MAX_JOUEUR];
    	
        //Instancier les données depuis la base de données
    	
    	
    	//on instancie le perso admin ou celui de teste je pense
    	joueur[0] = new Personnage("Admin");
    	
    	//on place le joueur � un emplacement par d�faut (x=18, y=1) sur la map
    	map1.setPerso(joueur[0], 18, 1);
    }


    public Personnage getChar(String charName) {
    	int i = 0;
    	boolean trouver = false;
    	
        while (i < MAX_JOUEUR && !trouver) {
        	trouver = joueur[i].getNomJoueur() == charName;
        	i++;
        }
        
        if (trouver)
        	return joueur[i-1];
        else
        	return null;
    }
    
    //mServeur.moveChar(mServeur.getzCharName(...), jsonObj.get("x"), jsonObj.get("y"));
    public boolean moveChar(String charName, int direction) {
        boolean charMoved = false;
        int column = 0, line = 0;
        int oldC = 0, oldL = 0;
        Personnage tempPerso;
        
        if ((tempPerso = getChar(charName)) == null) {
            System.out.println("Erreur moveChar: perso innexistant");
            return false;
        }
        else {
        	oldC = tempPerso.getX();
        	oldL = tempPerso.getY();
			column = oldC;
			line = oldL;
        	
        	switch(direction) {
		        case BAS :
		            line = line + 1;
					tempPerso.setDirection(BAS);
		            break;
		        case GAUCHE :
		            column = column - 1;
					tempPerso.setDirection(GAUCHE);
		            break;
		        case DROITE :
		        	column = column + 1;
					tempPerso.setDirection(DROITE);
		            break;
		        case HAUT :
		        	line = line - 1;
					tempPerso.setDirection(HAUT);
		            break;
        	}
        	if (charMoved = map1.setPerso(tempPerso, column, line))
				map1.removePerso(oldC, oldL);
        }
        
        return charMoved;
    }

	//"{\"phonetype\":\"N95\",\"cat\":\"WP\"}"
	public String getJSONCharCoord(String nomPerso) {
		String charListeJSON = null;
		Personnage tempChar;
		boolean trouver = false;
		int i = 0;

		while (i < MAX_JOUEUR && !trouver) {
			tempChar = joueur[i];

			if (!(tempChar == null)) {
				if (tempChar.getNomJoueur() == nomPerso) {
					trouver = true;
					charListeJSON = "\"" + tempChar.getNomJoueur() + "\" : {\"direction\":\"" + tempChar.getDirection() + "\", \"x\":\"" + tempChar.getX() + "\",\"y\":\"" + tempChar.getY() + "\"}";
				}
			}
		}

		return "{\"serverPacket\":\"majMap\",\"data\":{" + charListeJSON + "}}";
	}
	/*
	public String getJSONCharCoord(String nomPerso) { Pour mapLoad
		String charListeJSON = null;

		for (Personnage tempChar : joueur) {
			if (!(tempChar == null)) {
				if (!(charListeJSON == null)) {
					charListeJSON += ",";
				}
				else
					charListeJSON = "";
				//"admin": {"direction":"0", "x": "12", "y": "14"},
				charListeJSON += "\"" + tempChar.getNomJoueur() + "\" : {\"direction\":\"" + tempChar.getDirection() + "\", \"x\":\"" + tempChar.getX() + "\",\"y\":\"" + tempChar.getY() + "\"}";
			}
		}

		return "{\"serverPacket\":\"majMap\",\"data\":{" + charListeJSON + "}}";
	}*/
}

