import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import jdk.nashorn.internal.parser.JSONParser;
import model.ModelServeur;
import org.json.JSONException;
import org.json.*;
import com.google.gson.*;



@ServerEndpoint("/chatroomServer")

public class ChatRoomServerEndPoint {
    // Set of all the chatroom users
    static Set<Session> chatroomUsers = Collections.synchronizedSet(new HashSet<Session>());
    private ModelServeur mServeur = new ModelServeur();

    @OnOpen
    public void handleOpen(Session userSession) throws IOException {
        System.out.println("On est dans serveur: Open\n");
        chatroomUsers.add(userSession);
        for (Session chatroomUser : chatroomUsers) {
            (chatroomUser).getBasicRemote().sendText(buildJsonUsersData());
        }
    }

    @OnMessage
    public void handleMessage(String message, Session userSession) throws IOException {
        System.out.println("On est dans serveur: handleMessage\n");
        try {
            userSession.getUserProperties().put("username", "Admin"); // Temporaire
            String username = (String) userSession.getUserProperties().get("username");
            Iterator<Session> iterator = chatroomUsers.iterator();
            //com.google.gson.JsonObject jsonObj = new JsonParser().parse(message).getAsJsonObject();
            JSONObject jsonObj = new JSONObject(message);
            //String jsonTypeMsg = jsonObj.get("clientPacket").getAsString();
            String jsonTypeMsg = (String) jsonObj.get("clientPacket");

            if(username==null) { //Cette partie on l'a fera pas comme sa
                userSession.getUserProperties().put("username", message);
                userSession.getBasicRemote().sendText(buildJsonMessageData("System", "you are now connected as " + message));
                while (iterator.hasNext()) {
                    (iterator.next()).getBasicRemote().sendText(buildJsonUsersData());
                }
            }
            else {
                //Si le packet reçu est de tyoe "charGoTo" qui dit si un personnage peut se déplacer
                if (jsonTypeMsg.equals("charGoTo")) {
                    JSONObject jsonData = jsonObj.getJSONObject("data");
                    int teste = Integer.parseInt(jsonData.getString("direction"));
                    mServeur.moveChar(username, teste);

                    //On créer le json majMap pour les clients
                    String jsonToSend = mServeur.getJSONCharCoord(username);

                    //On met a jour l'affichage de tout les clients
                    while (iterator.hasNext())
                        iterator.next().getBasicRemote().sendText(jsonToSend);
                }
                else if (jsonTypeMsg.equals("chatMsg")) { //Pour les messages du chat, Pas fini
                    //String message = jsonObj.getJSONObject("data");

                    while (iterator.hasNext()) {
                        iterator.next().getBasicRemote().sendText(buildJsonMessageData(username, message));
                    }
                }
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @OnClose
    public void handleClose(Session userSession) throws IOException {
        chatroomUsers.remove(userSession);
        for (Session chatroomUser : chatroomUsers) {
            (chatroomUser).getBasicRemote().sendText(buildJsonUsersData());
        }
    }

    private String buildJsonUsersData() throws IOException {
        System.out.println("On appelle buildJsonUsersData\n");
        Iterator<String> iterator = getUserNames().iterator();
        JsonArrayBuilder jsonArrayBuilder = Json.createArrayBuilder();
        while (iterator.hasNext()) {
            jsonArrayBuilder.add(iterator.next());
        }
        return Json.createObjectBuilder().add("users", jsonArrayBuilder).build().toString();
    }

    /*private void convertJSONString(String myJson) {
        String Data=response.getEntity().getText().toString(); // reading the string value
        JSONObject json = (JSONObject) new JSONParser().parse(Data);
        String x=(String) json.get("phonetype");
        System.out.println("Check Data"+x);
        String y=(String) json.get("cat");
        System.out.println("Check Data"+y);
    }*/
    
    private String buildJsonMapData(String username, String message) {
        System.out.println("On appelle buildJsonMessageData\n");
        /*
        {"serverPacket":"majMap", 
        	"data":
        	{"nomPerso":"admin", "direction":"'+ DIRECTION.HAUT + '" }
        }*/
        JsonObject jsonObject = Json.createObjectBuilder().add("serverPacket", username+": "+message).build();
        StringWriter stringWriter = new StringWriter();

        try	(JsonWriter jsonWriter = Json.createWriter(stringWriter)) {
            jsonWriter.write(jsonObject);
        }
        return stringWriter.toString();
    }
    
    private String buildJsonMessageData(String username, String message) {
        System.out.println("On appelle buildJsonMessageData\n");
        JsonObject jsonObject = Json.createObjectBuilder().add("message", username+": "+message).build();
        StringWriter stringWriter = new StringWriter();

        try	(JsonWriter jsonWriter = Json.createWriter(stringWriter)) {
            jsonWriter.write(jsonObject);
        }
        return stringWriter.toString();
    }

    private Set<String> getUserNames() {
        HashSet<String> returnSet = new HashSet<>();
        for (Session chatroomUser : chatroomUsers) {
            returnSet.add(chatroomUser.getUserProperties().get("username").toString());
        }
        return returnSet;
    }
}
