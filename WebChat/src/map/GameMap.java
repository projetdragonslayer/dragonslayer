package map;

import java.io.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import personnage.Personnage;

/**
 * Created by Sandy on 25/04/2016.
 */
public class GameMap {
    private int nbColumn;
    private int nbLine;
    private Case_map mesCase[][];

    public GameMap(int column, int line) {
    	// par d�faut, 38 et 18
        nbColumn = column;
        nbLine = line;
        mesCase = new Case_map[nbLine][nbColumn];
    }
    
    public void loadMapAsset(String map) {
    	int col, line;
    	String jsonRawData = "";
    	String ligne;
		
		try {
			//room = ImageIO.read(getClass().getResource("/room.png"));
			//InputStream in = FileLoadder.class.getResourceAsStream("C:\\Users\\Sandy\\Documents\\dragonslayer\\WebChat\\web\\maps\\" + map + ".json");
			//String filePath = new File("C:\\Users\\Sandy\\Documents\\dragonslayer\\WebChat\\web\\maps\\").getAbsolutePath();


			// FAUT REMPLACER LE CHEMIN QUE J'AI MIS PAR CELUI DU DOSSIER Où YA LES MAPS D'ANTON
			//BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\Sandy\\Documents\\dragonslayer\\WebChat\\web\\maps\\" + map + ".json"));

			//BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\Anton\\OneDrive\\IUT\\Projet DragonSlayer\\DragonSlayer\\web\\maps\\" + map + ".json"));

			//Normalement, sa sa marche
			String filePath = getClass().getResource("").getPath();
			//filePath = filePath.replace("/WEB-INF/classes/map", "");
			//filePath += "maps/"  + map + ".json";
			filePath = filePath.replace("/out/artifacts/DragonSlayer_war_exploded/WEB-INF/classes/map", "");
			filePath += "web/maps/"  + map + ".json";
			BufferedReader reader = new BufferedReader(new FileReader(filePath));

			while ((ligne = reader.readLine()) != null) {
				jsonRawData += ligne;
			}
			reader.close();
			
			JSONObject jsonObj = new JSONObject(jsonRawData);
			
	        JSONArray jsonData = jsonObj.getJSONArray("terrain");

			/*if (map.equals("dungeon")) { A prendre en compte si on a différente map à charger
				caseAsansCollision = 1;
				caseBsansCollision = 66;
			}*/


			for (int i = 0; i < jsonData.length(); ++i) {
				JSONArray jsonLine = jsonData.getJSONArray(i);

				for (int j = 0; j < jsonLine.length(); ++j) {
					if (jsonLine.getInt(j) == 1 || jsonLine.getInt(j) == 66)
						mesCase[i][j] = new Case_map(false);
					else
						mesCase[i][j] = new Case_map(true);
				}

			}
		} 
        catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (FileNotFoundException fe) {
			fe.printStackTrace();
		}
		catch (UnsupportedEncodingException ue) {
			ue.printStackTrace();
		}
		catch (IOException e) {
			System.err.format("Exception occurred trying to read '%s'.", "default.txt");
			e.printStackTrace();
		}
    }
    
    public boolean setPerso(Personnage perso, int column, int line) {
    	if (mesCase[line][column].setPersonnage(perso)) {
			perso.setPos(column, line);
			return true;
		}
		else
			return false;

    }

	public void removePerso(int column, int line) {
		mesCase[line][column].removePersonnage();
	}

    public Case_map getCaseByIndex (int column, int line) {
    	return mesCase[line][column];
    }
}
