package map;

import personnage.Personnage;

/**
 * Created by Sandy on 25/04/2016.
 */
public class Case_map {
	protected boolean collision;
	protected Personnage perso;
	
	public Case_map () {
		collision = false;
	}
	
	public Case_map (boolean collision) {
		this.collision = collision;
	}
	
	public void setCollision(boolean collision) {
		this.collision = collision;
	}
	
	public boolean setPersonnage(Personnage perso) {
		if (collision) 
			return false;
		else {
			this.perso = perso;
			collision = true;
			return true;
		}
	}

	public void removePersonnage() {
		perso = null;
		collision = false;
	}
}
