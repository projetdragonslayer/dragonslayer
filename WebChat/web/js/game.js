/**
    * Created by Anton on 19/04/2016.
    */

var map = new Map("dungeon");

var joueur = new Personnage("test.png", 18, 1, DIRECTION.BAS);
map.addPersonnage(joueur);


window.onload = function() {
    // On récupère l'élément DOM du canvas
    var canvas = document.getElementById('canvas');

    // fournit la bibiliotheque 2D en gros
    var ctx = canvas.getContext('2d');

    canvas.width  = map.getLargeur() * 32;
    canvas.height = map.getHauteur() * 32;

    //On redessine la carte tous les 40 millisecondes (25frames/s)
    setInterval(function() {
        map.dessinerMap(ctx);
    }, 40);

    // Gestion du clavier
    window.onkeydown = function(event) {

        // On récupère le code de la touche (deux commandes pour les différents navigateurs)
        var e = event || window.event;
        var key = e.which || e.keyCode;
        
        switch(key) {
            case 38 : // Flèche haut
            //joueur.deplacer(DIRECTION.HAUT, map);
                var jsonKey  = '' + DIRECTION.HAUT + '';
                var message = JSON.stringify({"clientPacket":"charGoTo", "data":{"nomPerso":"Admin", "direction":jsonKey}});
                websocket.send(message);
                break;
            case 40 : // Flèche bas
            //joueur.deplacer(DIRECTION.BAS, map);
                var jsonKey  = '' + DIRECTION.BAS + '';
                var message = JSON.stringify({"clientPacket":"charGoTo", "data":{"nomPerso":"Admin", "direction":jsonKey}});
                websocket.send(message);
            break;
            case 37 : // Flèche gauche
            //joueur.deplacer(DIRECTION.GAUCHE, map);
                var jsonKey  = '' + DIRECTION.GAUCHE + '';
                var message = JSON.stringify({"clientPacket":"charGoTo", "data":{"nomPerso":"Admin", "direction":jsonKey}});
                websocket.send(message);
            break;
            case 39 : // Flèche droite
            //joueur.deplacer(DIRECTION.DROITE, map);
                var jsonKey  = '' + DIRECTION.DROITE + '';
                var message = JSON.stringify({"clientPacket":"charGoTo", "data":{"nomPerso":"Admin", "direction":jsonKey}});
                websocket.send(message);
            break;
            default :
                //alert(key);
                // Si la touche ne nous sert pas, nous n'avons aucune raison de bloquer son comportement normal.
                return true;
        }


    }

};
