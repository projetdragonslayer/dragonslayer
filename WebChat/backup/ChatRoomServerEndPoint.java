import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import jdk.nashorn.internal.parser.JSONParser;
import model.ModelServeur;
import org.json.JSONException;
import org.json.*;
import com.google.gson.*;



@ServerEndpoint("/chatroomServer")

public class ChatRoomServerEndPoint {
    // Set of all the chatroom users
    static Set<Session> chatroomUsers = Collections.synchronizedSet(new HashSet<Session>());
    private ModelServeur mServeur = new ModelServeur();

    @OnOpen
    public void handleOpen(Session userSession) throws IOException {
        System.out.println("On est dans serveur: Open\n");
        chatroomUsers.add(userSession);
        for (Session chatroomUser : chatroomUsers) {
            (chatroomUser).getBasicRemote().sendText(buildJsonUsersData());
        }
    }

    @OnMessage
    public void handleMessage(String message, Session userSession) throws IOException, JSONException {
        System.out.println("On est dans serveur: handleMessage\n");
        userSession.getUserProperties().put("username", "Admin");
        String username = (String) userSession.getUserProperties().get("username");
        Iterator<Session> iterator = chatroomUsers.iterator();
        //com.google.gson.JsonObject jsonObj = new JsonParser().parse(message).getAsJsonObject();
        JSONObject jsonObj = new JSONObject(message); // Ne marche pas, je doit trouver pourquoi
        //String jsonTypeMsg = jsonObj.get("clientPacket").getAsString();
        String jsonTypeMsg = (String) jsonObj.get("clientPacket");

        if(username==null) { //Cette partie on l'a fera plus comme sa: inscrire le nom du joueur associé à sa session
            userSession.getUserProperties().put("username", message);
            userSession.getBasicRemote().sendText(buildJsonMessageData("System", "you are now connected as " + message));
            while (iterator.hasNext()) {
                (iterator.next()).getBasicRemote().sendText(buildJsonUsersData());
            }
        }
        else {
            //Si le packet reçu est de tyoe "charGoTo" qui dit si un personnage peut se déplacer
            if (jsonTypeMsg.equals("charGoTo")) {
                //On utilisera surment la fonction MoveChar de mServeur. Faudra passer en parametre le nom du perso, et ses nouvelle coord xy
                //La fonction sera boolean et retournera vraie si le perso a été déplacer ou faux sinon
                //donc l'utilisation de la fonction ressemblera à un truc comme sa:
                //if (mServeur.moveChar(username, jsonObj.get("direction").getAsInt())) {
                if (mServeur.moveChar(username, (int) jsonObj.get("direction"))) {
                    //On met a jour l'affichage de tout les clients
                    iterator.next().getBasicRemote().sendText("");
                }
            }
            else if (jsonTypeMsg.equals("chatMsg")) { //Pour les messages du chat
                while (iterator.hasNext()) {
                    iterator.next().getBasicRemote().sendText(buildJsonMessageData(username, message));
                }
            }
        }
    }

    @OnClose
    public void handleClose(Session userSession) throws IOException {
        chatroomUsers.remove(userSession);
        for (Session chatroomUser : chatroomUsers) {
            (chatroomUser).getBasicRemote().sendText(buildJsonUsersData());
        }
    }

    private String buildJsonUsersData() throws IOException {
        System.out.println("On appelle buildJsonUsersData\n");
        Iterator<String> iterator = getUserNames().iterator();
        JsonArrayBuilder jsonArrayBuilder = Json.createArrayBuilder();
        while (iterator.hasNext()) {
            jsonArrayBuilder.add(iterator.next());
        }
        return Json.createObjectBuilder().add("users", jsonArrayBuilder).build().toString();
    }

    /*private void convertJSONString(String myJson) {
        String Data=response.getEntity().getText().toString(); // reading the string value
        JSONObject json = (JSONObject) new JSONParser().parse(Data);
        String x=(String) json.get("phonetype");
        System.out.println("Check Data"+x);
        String y=(String) json.get("cat");
        System.out.println("Check Data"+y);
    }*/
    
    private String buildJsonMapData(String username, String message) {
        System.out.println("On appelle buildJsonMessageData\n");
        /*
        {"serverPacket":"majMap", 
        	"data":
        	{"nomPerso":"admin", "direction":"'+ DIRECTION.HAUT + '" }
        }*/
        JsonObject jsonObject = Json.createObjectBuilder().add("serverPacket", username+": "+message).build();
        StringWriter stringWriter = new StringWriter();

        try	(JsonWriter jsonWriter = Json.createWriter(stringWriter)) {
            jsonWriter.write(jsonObject);
        }
        return stringWriter.toString();
    }
    
    private String buildJsonMessageData(String username, String message) {
        System.out.println("On appelle buildJsonMessageData\n");
        JsonObject jsonObject = Json.createObjectBuilder().add("message", username+": "+message).build();
        StringWriter stringWriter = new StringWriter();

        try	(JsonWriter jsonWriter = Json.createWriter(stringWriter)) {
            jsonWriter.write(jsonObject);
        }
        return stringWriter.toString();
    }

    private Set<String> getUserNames() {
        HashSet<String> returnSet = new HashSet<>();
        for (Session chatroomUser : chatroomUsers) {
            returnSet.add(chatroomUser.getUserProperties().get("username").toString());
        }
        return returnSet;
    }
}
