package personnage;

import model.ModelServeur;

public class Personnage {
	private String nom;
	private int direction;
	private int x;
	private int y;
	
	public Personnage (String nom) {
		this.nom = nom;
		direction = ModelServeur.BAS;
		x = 18;
		y = 1;
	}
	
	public void setPos(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public String getNomJoueur() {
		return nom;
	}
}
