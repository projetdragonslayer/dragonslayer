package map;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import personnage.Personnage;

/**
 * Created by Sandy on 25/04/2016.
 */
public class GameMap {
    private int nbColumn;
    private int nbLine;
    private Case_map mesCase[][];

    public GameMap(int column, int line) {
    	// par d�faut, 37 et 17
        nbColumn = column;
        nbLine = line;
        mesCase = new Case_map[nbLine][nbColumn];
    }
    
    public void loadMapAsset(String map) {
    	int col, line;
    	String jsonRawData = "";
    	String ligne;
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\Sandy\\Documents\\dragonslayer\\WebChat\\web\\maps\\" + map + ".json"));

			while ((ligne = reader.readLine()) != null) {
				jsonRawData += ligne;
			}
			reader.close();
			
			JSONObject jsonObj = new JSONObject(jsonRawData);
			
	        JSONArray jsonData = jsonObj.getJSONArray("terrain");
	        
			for (int i = 0; i < jsonData.length(); ++i) {
				JSONArray jsonLine = jsonData.getJSONArray(i);

				for (int j = 0; j < jsonLine.length(); ++j) {
					if (jsonLine.getInt(j) == 1 || jsonLine.getInt(j) == 66)
						mesCase[i][j] = new Case_map(false);
					else
						mesCase[i][j] = new Case_map(true);
				}

			}
		} 
        catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (FileNotFoundException fe) {
			fe.printStackTrace();
		}
		catch (UnsupportedEncodingException ue) {
			ue.printStackTrace();
		}
		catch (IOException e) {
			System.err.format("Exception occurred trying to read '%s'.", "default.txt");
			e.printStackTrace();
		}
    }
    
    public boolean setPerso(Personnage perso, int column, int line) {
    	return mesCase[line][column].setPersonnage(perso);
    }
    
    public Case_map getCaseByIndex (int column, int line) {
    	return mesCase[line][column];
    }
}
